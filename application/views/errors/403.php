[menu]
<!-- Navigation -->
<!-- Navigation -->
<section class="navigation navigation--bgf8">
  <div class="container clearfix">
    <div class="row">
      <div class="col-md-12">
        <h2>
          404 - Página no encontrada
        </h2>
        <ul class="breadcrumbs ul--inline ul--no-style">
          <li>
            <a href="<?= base_url() ?>">Inicio</a>
          </li>
          <span>/</span>
          <li class="active">
            404
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!-- End Navigation -->
<!-- 404 page -->
<section class="page-404">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6 col-md-12">
        <div class="big-info">
          <span>
            404
          </span>
          	Pagina no encontrada
        </div>
        <p>
          La url que desea buscar no se encuentra disponible o no existe en este sitio, Verifique la url e intente nuevamente.
        </p>
        <div class="see-more">
          <a href="<?= base_url() ?>" class="au-btn au-btn--big au-btn--pill au-btn--yellow au-btn--white">Inicio</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End 404 page -->
<!-- End Contact Info -->
[footer]
[scripts]