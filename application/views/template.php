<!doctype html>
<html lang="en">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': $description ?>" /> 	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/editor.css">
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
	<script>var URL = '<?= base_url() ?>';</script>	

	
   	<!-- ================== Font =================== -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/font/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/font/mdi-font/css/material-design-iconic-font.min.css">
	<!-- ================== Vendor CSS =================== -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/vendor/bootstrap4/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/vendor/owl-carousel/animate.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/vendor/owl-carousel/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/vendor/owl-carousel/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/vendor/revolution/settings.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/vendor/revolution/navigation.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/vendor/revolution/layers.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/vendor/lightbox2/src/css/lightbox.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/external.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/vendor/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/vendor/slick/slick-theme.css">

	<!-- Main CSS -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/font.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/style_custom.css">
</head>
<body data-rsssl="1" class="home page-template-default page page-id-2039 gdlr-core-body woocommerce-no-js realfactory-body realfactory-body-front realfactory-full  realfactory-with-sticky-navigation gdlr-core-link-to-lightbox">
<?php 
	if(empty($editor)){
		$this->load->view($view); 
	}else{
		echo $view;
	}
?>

<script src="https://www.google.com/recaptcha/api.js?render=6LebvK8UAAAAALEbZAjEUqTo3cC8mmnVOJh6oVRb"></script>
<script>
grecaptcha.ready(function() {
  grecaptcha.execute('6LebvK8UAAAAALEbZAjEUqTo3cC8mmnVOJh6oVRb', {action: 'homepage'}).then(function(token) {
    console.log(token);
     $(document).find("input[name='token']").val(token);
  });
});
</script>
</body>
</html>