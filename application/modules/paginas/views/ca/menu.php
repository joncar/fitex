<!-- Page Loader -->
<div id="page-loader">
  <div class="page-loader__inner">
    <div class="page-loader__spin"></div>
  </div>
</div>
<!-- End Page Loader -->
<!-- Page Wrap -->
<div class="page-wrap">
  <!-- Header Stick -->
  <header class="header-stick header-stick--dark">
    <div class="container clearfix">
      <h1 class="logo pull-left">
      <a href="<?= base_url() ?>">        
        <img alt="Logo" src="<?= base_url() ?>theme/theme/img/logo-white.png">
      </a>
      </h1>
      <nav class="menu-desktop pull-right">
        <ul class="ul--inline ul--no-style">
          <?php $this->load->view($this->theme.'_menu',array(),FALSE,'paginas'); ?>
        </ul>
      </nav>
    </div>
  </header>
  <!-- End Header Stick -->
  <!-- Header Desktop -->
  <header class="header-desktop header1">
    <div class="container clearfix">
      <h1 class="logo pull-left">
        <a href="<?= base_url() ?>">
          <img alt="logo" src="<?= base_url() ?>theme/theme/img/logo-black.svg">
        </a>
      </h1>
      <div class="header-button pull-right clearfix">
        <div class="canvas-menu-button pull-right">
          <a href="#" onclick="return false;">
            <i class="zmdi zmdi-menu"></i>
          </a>
        </div>
        <div class="mini-cart pull-right">
          <a href="<?= base_url('main/traduccion/ca') ?>" <?= $_SESSION['lang']=='ca'?'style="color:lightgray"':'' ?>>
            CAT
          </a> |
          <a href="<?= base_url('main/traduccion/es') ?>" <?= $_SESSION['lang']=='es'?'style="color:lightgray"':'' ?>>
            CAS
          </a> |
          <a href="<?= base_url('main/traduccion/en') ?>" <?= $_SESSION['lang']=='en'?'style="color:lightgray"':'' ?>>
            ENG
          </a>        
        </div>
        <div class="search-button pull-right clearfix">
          <a class="search-button__btn" href="#" onclick="return false;">
            <i class="zmdi zmdi-search"></i>
          </a>
          <form class="form form-header pull-left" role="form" action="paginas/frontend/search" method="get">
            <input type="text" name="q" id="header-input" class="form__input form__input--hidden" placeholder="Què Busques?">
          </form>
        </div>
      </div>
      <nav class="menu-desktop menu-desktop--show pull-right">
        <ul class="ul--inline ul--no-style">
          <?php $this->load->view($this->theme.'_menu',array(),FALSE,'paginas'); ?>
        </ul>
      </nav>
    </div>
  </header>
  <!-- End Header Desktop -->
  <!-- Menu Canvas -->
  <div id="menu-canvas" class="menu-canvas--hidden">
    <div class="menu-canvas__inner">
      <div class="close-menu-canvas">
        <i class="zmdi zmdi-close" id="btn-close"></i>
      </div>
      <h1 class="logo">
      <a href="<?= base_url() ?>">
        <img alt="Logo" src="<?= base_url() ?>theme/theme/img/logo-white.png">
      </a>
      </h1>
      <div class="menu-canvas-slide">
        <div class="menu-canvas__image">
          <div id="sync1" class="owl-carousel owl-theme">
            <div class="item">
              <img alt="Project 1" src="<?= base_url() ?>theme/theme/img/item-menu-canvas-01.jpg">
            </div>
            <div class="item">
              <img alt="Project 2" src="<?= base_url() ?>theme/theme/img/item-menu-canvas-02.jpg">
            </div>
          </div>
        </div>
        <div id="sync2" class="owl-carousel owl-theme">
          <p class="item menu-canvas__detail">
            FITEX és la marca de la Fundació Privada per a la Innovació Tèxtil , una entitat privada sense ànim de lucre que té com a objectiu fonamental impulsar el procés d'innovació de la indústria del gènere de punt.

Des de la seva creació l'any 2000, FITEX ha esdevingut un centre de referència per les empreses que realitzen R+D+I lligada al sector tèxtil-moda-indumentària.

La missió de FITEX és impulsar la innovació en sentit ampli: producte, procés, gestió, coneixement i estratègia comercial a les empreses tèxtils.
          </p>
          <p class="item menu-canvas__detail">
            Per FITEX ajudar a innovar és integrar-se a la cadena de valor de les seves empreses usuàries per tal de generar satisfacció al seus clients i millorar els seus resultats.
FITEX estructura els processos d'innovació mitjançant àrees de servei a les empreses: la formació, projectes de investigació i desenvolupament, difusió de novetats tècniques i tecnològiques i serveis a empreses.
          </p>
        </div>
      </div>
      <div class="social">
        <!-- 
<a href="" class="social__item">
          <i class="zmdi zmdi-facebook"></i>
        </a> 
 -->       
        <a href="https://twitter.com/FitexIGD" class="social__item">
          <i class="zmdi zmdi-twitter"></i>
        </a>
        <!-- 
<a href="" class="social__item">
          <i class="zmdi zmdi-instagram"></i>
        </a>
 -->
      </div>
    </div>
  </div>
  <!-- End Menu Canvas -->
  <!-- Header Mobile -->
  <header class="header-mobile">
    <div class="container clearfix">
      <h1 class="logo pull-left">
      <a href="<?= base_url() ?>">
        <img alt="Logo" src="<?= base_url() ?>theme/theme/img/logo-black.png">
      </a>
      </h1>
      <a class="menu-mobile__button">
        <i class="fa fa-bars"></i>
      </a>
      <nav class="menu-mobile hidden">
        <ul class="ul--no-style">
          <li>            
            <a href="<?= base_url() ?>qui-som.html">
              Qui som
            </a>          
          </li>
          <li>
            <i class="fa fa-plus menu-mobile__more"></i>
            <a href="<?= base_url() ?>projectes.html">
              Projectes
            </a>
            <ul class="ul--no-style hidden">
              <li>
                <a href="<?= base_url() ?>recerca-i-desenvolupament.html">
                  Recerca i Desenvolupament
                </a>
              </li>  
              
              <li>
                <a href="<?= base_url() ?>innovacio.html">
                  Innovació
                </a>
              </li>
               <li>
                <a href="<?= base_url() ?>internacionalitzacio.html">
                  Internacionalització
                </a>
              </li>
            </ul>
          </li>
          <li>
            <i class="fa fa-plus menu-mobile__more"></i>
            <a href="<?= base_url() ?>serveis.html">
              Serveis
            </a>
            <ul class="ul--no-style hidden">
              <?php foreach($this->elements->servicios()->result() as $s): ?>
                <li>
                  <a href="<?= $s->url ?>">
                    <?= $s->nombre ?>
                  </a>
                </li>
              <?php endforeach ?>
            </ul>
          </li>
          <li>
            <i class="fa fa-plus menu-mobile__more"></i>
            <a href="<?= base_url() ?>formacio.html">
              Formació
            </a>
            <ul class="ul--no-style hidden">
              <li>
                <a href="<?= base_url() ?>formacio.html" style="text-transform:capitalize">
                  Tots formacions
                </a>
              </li>
              <li>
                <a href="<?= base_url() ?>formacio-detail.html" style="text-transform:capitalize">
                  Àrea Técnica i tecnológica
                </a>
              </li>
              <li>
                <a href="<?= base_url() ?>formacio-detail.html" style="text-transform:capitalize">
                  Àrea d’estrategia i innovació
                </a>
              </li>
              <li>
                <a href="<?= base_url() ?>formacio-detail.html" style="text-transform:capitalize">
                  Àrea d’internacionalització
                </a>
              </li>
              <li>
                <a href="<?= base_url() ?>formacio-detail.html" style="text-transform:capitalize">
                  Àrea TIC’s
                </a>
              </li>
              <li>
                <a href="<?= base_url() ?>formacio-detail.html" style="text-transform:capitalize">
                  Àrea de gestió RRHH
                </a>
              </li>
              <li>
                <a href="<?= base_url() ?>formacio-detail.html" style="text-transform:capitalize">
                  Àrea retail i branding
                </a>
              </li>
              <li>
                <a href="<?= base_url() ?>formacio-detail.html" style="text-transform:capitalize">
                  Àrea comerç, marqueting i comunicació
                </a>
              </li>
            </ul>
          </li>
          <li>            
            <a href="<?= base_url() ?>blog">
              Notícies
            </a>            
          </li>          
          <li>
            <a href="<?= base_url() ?>contacte.html">
              Contacte
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </header>
  <!-- End Header Mobile -->