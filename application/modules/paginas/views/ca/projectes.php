[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--ser-li">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= l('projectes') ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]"><?= l('inici') ?></a>
                </li>
                <span>/</span>                
                <li class="active">
                  <?= l('projectes') ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- Service List -->
    <section class="service-list">
      <div class="container">
        <div class="row">
          
        <?php foreach($this->elements->categoria_proyectos()->result() as $s): ?>
          <div class="col-lg-4 col-md-6">
            <div class="service-list__item img-blog">
              <a href="<?= $s->url ?>">
                <img alt="Service 1" src="<?= $s->miniatura ?>">
              </a>
              <div class="service-list__text">
                <h5>
                  <a href="<?= $s->url ?>"><?= $s->nombre ?></a>
                </h5>
                <p>
                  <?= cortar_palabras(strip_tags($s->descripcion),20) ?>
                </p>
                <br/>
                <a href="<?= $s->url ?>" class="au-btn au-btn--pill au-btn--yellow au-btn--white"><?= l('Veure projecte') ?></a>
              </div>
            </div>
          </div>
        <?php endforeach ?>
          

        </div>
      </div>
      <?php $this->load->view($this->theme.'_contacte_bar_2'); ?>
    </section>
    <!-- End Service List -->
[footer]
[scripts]