[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--nav-1">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= l('Resultats de busqueda') ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]">Fitex</a>
                </li>
                <span>/</span>
                <li class="active">
                  <?= l('Resultats de busqueda') ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- We Are -->
    <section class="we-are">
      <div class="container">
        <div class="row no-gutters">
          <div class="col-lg-12 col-md-12 col-12 searchResults">
          	<?= $resultado ?>            
          </div>
        </div>
      </div>
    </section>
    <!-- End we are -->
    <!-- Testi-Partner -->
    <section class="testi-partner">
      <div class="container">
        <div class="row">
          
          <div class="col-lg-12 col-md-12 col-12">
            <div class="testi-partner__right">
              <h2 class="title title-3 title-3--left">
                <?= l('Col·laboradors') ?>
              </h2>
              <div class="partner-wrap1 owl-carousel owl-theme" id="owl-partner-2">
                <a href="" class="partner__item item">
                  <img alt="Partner 1" src="[base_url]theme/theme/img/partner-05.jpg">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 2" src="[base_url]theme/theme/img/partner-06.jpg">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 3" src="[base_url]theme/theme/img/partner-07.jpg">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 4" src="[base_url]theme/theme/img/partner-08.jpg">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 5" src="[base_url]theme/theme/img/partner-11.jpg">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 6" src="[base_url]theme/theme/img/partner-07.jpg">
                </a>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Testi-Partner -->
 <?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
    [footer]
    [scripts]
