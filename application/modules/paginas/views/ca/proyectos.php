<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>
 <!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--ser-li">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= $detail->nombre ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]"><?= l('inici') ?></a>
                </li>
                <span>/</span>                
                <li class="active">
                 <?= $detail->nombre ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
<!-- Service Content -->
<section class="service-content p-t-50">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-4">
				<div class="service-tab">
					<ul class="ul--no-style">
						
						<?php foreach($this->elements->get_proyectos()->result() as $p): ?>
							<li <?= $p->id==$detail->id?' class="active"':'' ?>>
								<a href="<?= $p->url ?>">
									<?= $p->nombre ?>
								</a>
							</li>
						<?php endforeach ?>
						
					</ul>
				</div>
			</div>
			<div class="col-lg-9 col-md-8">
				<div class="service-text m-t-50">					
					<?= $detail->descripcion ?>										
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Service Content -->
<!-- Our Process -->
<section class="process-page">  <h2 class="title title-3 title-3--center">
	<?= l('projectes') ?> <?= $detail->nombre ?>
	</h2>
	<div class="container">
		
		<div class="row">			
			<?php foreach($detail->proyectos->result() as $n=>$p): ?>
				<div class="col-lg-4 col-md-6">
		            <div class="service-list__item img-blog">
		            	<a href="<?= $p->url ?>">
			              <img alt="Service 1" src="<?= $p->foto ?>"></a>
			              <div class="service-list__text">
			                <h5>
			                  <a href="<?= $p->url ?>"><?= $p->titulo ?></a>
			                </h5>			                
			                <p>
			                  <?= empty($p->subtitulo)?cortar_palabras(strip_tags($p->descripcion),20):$p->subtitulo; ?> 			                  
			                </p>
			                <br>
			                <a href="<?= $p->url ?>" class="au-btn au-btn--pill au-btn--yellow au-btn--white"><?= l('Veure més') ?></a>
			              </div>
		            </div>
	          </div>
			<?php endforeach ?>
		</div>

	</div>
</section>
<!-- Our Process -->
<!-- End Blog Grid 1 -->
<?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>