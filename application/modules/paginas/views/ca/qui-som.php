[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--nav-1">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= l('qui som') ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]">Fitex</a>
                </li>
                <span>/</span>
                <li class="active">
                  <?= l('qui som') ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- We Are -->
    <section class="we-are">
      <div class="container">
        <div class="row no-gutters">
          <div class="col-lg-5 col-md-12 col-12">
            <div class="we-are__left">
              <div class="top">
                <div class="we-are__item">
                  <img alt="We Are 1" src="[base_url]theme/theme/img/we-are-01.jpg">
                </div>
                <div class="we-are__item">
                  <img alt="We Are 2" src="[base_url]theme/theme/img/we-are-02.jpg">
                </div>
              </div>
              <div class="bottom">
                <div class="we-are__item">
                  <img alt="We Are 3" src="[base_url]theme/theme/img/we-are-03.jpg">
                </div>
                <div class="we-are__item">
                  <img alt="We Are 4" src="[base_url]theme/theme/img/we-are-06.jpg">
                </div>
              </div>

            </div>
          </div>
          <div class="col-lg-7 col-md-12 col-12">
            <div class="we-are__right">
              <h2><?= l('qui som') ?></h2>
              <h2 class="title--small"></h2>

              <h5></h5>
              <p class="m-b-10">
                <?= l('qui som text') ?>                
              </p>
              <p>
              <br>
               <h4><?= l('Acreditacions') ?></h4><br>
               

<div class="li-item">
<i class="fa fa-check-circle" aria-hidden="true"></i>
<?= l('Centre col·laborador del Departament de Treball de la Generalitat de Catalunya') ?>
</div>
<div class="li-item">
<i class="fa fa-check-circle" aria-hidden="true"></i>
<?= l('Centre dR+D registrat al Ministerio de Industria') ?>
</div>

              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End we are -->
    <!-- Our Process 2 -->
    <section class="our-process2">
      <div class="overlay overlay--light">
      </div>
      <div class="parallax parallax--our-process2">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title title-3 title--light">
                <?= l('ÀMBITS') ?>
              </h2>
            </div>
          </div>
          <div class="row">
            
            
            
            <div class="col-lg-3 col-md-6 col-12">
              <a href="[base_url]recerca-i-desenvolupament.html">
                <div class="our-process2__item wow zoomIn" data-wow-delay=".6s">
                <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-03.png" >
                  <h4>
                    <span>
                      01<br>
                    </span>
                    <?= l('Recerca') ?>
                  </h4>
                  <p>
                    
                  </p>
                </div>
              </a>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
              <a href="[base_url]innovacio.html">
                <div class="our-process2__item wow zoomIn" data-wow-delay=".2s">
                <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-01.png" >
                  <h4>
                    <span>
                      02<br>
                    </span>
                   <?= l('innovacio') ?>
                  </h4>
                  
                </div>
              </a>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
              <a href="[base_url]internacionalitzacio.html">
                  <div class="our-process2__item wow zoomIn">
                  <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-112.png">
                    <h4>
                      <span>
                        03<br>
                      </span>
                      <?= l('internacionalitzacio') ?>
                    </h4>
                    <p>
                      
                    </p>
                  </div>
              </a>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
              <a href="[base_url]formacio.html">
                <div class="our-process2__item wow zoomIn" data-wow-delay=".4s">
                <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-02.png">
                  <h4>
                    <span>
                      04<br>
                    </span>
                    <?= l('formacio') ?>
                  </h4>
                  <p>
                    
                  </p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Our Process 2 -->
    <!-- Testi-Partner -->
    <section class="testi-partner">
      <div class="container">
        <div class="row">
          
          <div class="col-lg-12 col-md-12 col-12">
            <div class="testi-partner__right">
              <h2 class="title title-3 title-3--left">
                <?= l('Col·laboradors') ?> 
              </h2>
              <div class="partner-wrap1 owl-carousel owl-theme" id="owl-partner-2">
                <a href="" class="partner__item item">
                  <img alt="Partner 1" src="[base_url]theme/theme/img/partner-05.jpg">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 2" src="[base_url]theme/theme/img/partner-06.jpg">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 3" src="[base_url]theme/theme/img/partner-07.jpg">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 4" src="[base_url]theme/theme/img/partner-08.jpg">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 5" src="[base_url]theme/theme/img/partner-11.jpg">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 6" src="[base_url]theme/theme/img/partner-07.jpg">
                </a>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Testi-Partner -->
 <?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
    [footer]
    [scripts]