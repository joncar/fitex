 <!-- Footer -->
  <footer>
    <div class="overlay overlay--dark">
    </div>
    <div class="parallax parallax--footer">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-7 col-md-9 col-12">
            <h1>
            <a href="<?= site_url() ?>">
              <img alt="Logo" src="<?= base_url() ?>theme/theme/img/logo-white.png">
            </a>
            </h1>
            <ul class="ul--footer" style="margin-bottom: -30px;">              
              <li style="color:#fff"><?= l('Si vols estar informat deixans el teu correu') ?></li>
            </ul>
            <form method="post" class="form-footer" action="paginas/frontend/subscribir" onsubmit="sendForm(this,'.subscribeResponse'); return false;">
              <div class="subscribeResponse"></div>
              <input type="email" name="email" class="form__input" placeholder="El teu email">
              <button type="submit" class="au-btn au-btn--yellow au-btn--white au-btn--submit"><?= l('Enviar') ?></button>
            </form>
            <ul class="ul--inline ul--footer">
              <li>
                <a href="<?= base_url() ?>"><?= l('inici') ?></a>
              </li>
              <li>
                <a href="<?= base_url() ?>qui-som.html"><?= l('qui som') ?></a>
              </li>
              <li>
                <a href="<?= base_url() ?>serveis.html"><?= l('serveis') ?></a>
              </li>
              <li>
                <a href="<?= base_url() ?>blog"><?= l('noticies') ?></a>
              </li>
              <li>
                <a href="<?= base_url() ?>contacte.html"><?= l('Contacte') ?></a>
              </li>
              <li>
                <a href="<?= base_url() ?>avis-legal.html"><?= l('avis legal') ?></a>
              </li>
              <li>
                <a href="<?= base_url() ?>comptes-anuals.html"><?= l('Comptes Anuals') ?></a>
              </li>
            </ul>
            <div class="social">
              <!-- 
<a href="" class="social__item social__item~~circle">
                <i class="zmdi zmdi-facebook"></i>
              </a>
 -->
              <a href="https://twitter.com/FitexIGD" class="social__item social__item--circle">
                <i class="zmdi zmdi-twitter"></i>
              </a>
              <!-- 
<a href="" class="social__item social__item~~circle">
                <i class="zmdi zmdi-instagram"></i>
              </a>
 -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- End Footer -->
  <!-- Copyright -->
  <section class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          Copyright © <?= date("Y") ?> Diseñado por
          <span><a href="http://www.jordimagana.com">Jordi Magaña</a></span> . Todos los derechos reservados
        </div>
      </div>
    </div>
  </section>
  <!-- End Copyright -->
  <!-- Back to top -->
  <a href="" id="btn-to-top">
    <i class="fa fa-chevron-up"></i>
  </a>
  <!-- End Back to top -->
</div>
<!-- End Page Wrap -->