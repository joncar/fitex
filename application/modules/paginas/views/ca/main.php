[menu]
  <!-- Slider -->
  <section class="slide">
    <!-- revolution slider begin -->
    <div class="rev_slider_wrapper">
      <div id="revolution-slider1" class="rev_slider" data-version="5.4.4" style="display: none;">
        <ul>
          <li data-transition="fade" data-slotamount="7" data-masterspeed="2200" data-delay="6000">
            <!--  BACKGROUND IMAGE -->
            <img alt="Slide 1" src="[base_url]theme/theme/img/slide-01.jpg">
            <div class="tp-caption slide-title" data-x="center" data-y="['325', '210', '210', '180']" data-fontsize="['36', '36', '26', '24']"
              data-whitespace="normal" data-textAlign="['center']" data-frames='[{"delay":1600,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
              <?= l('Profesionals') ?>
            </div>
            <div class="tp-caption slide-content" data-x="center" data-y="center" data-width="['1170', '970', '768', '480']" data-fontsize="['72', '72', '62', '46']"
              data-textAlign="['center']" data-frames='[{"delay":1800,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'>
              <?= l('del sector textil') ?>
            </div>
            <a href="[base_url]contacte.html" class="tp-caption au-btn au-btn--big au-btn--pill au-btn--yellow au-btn--slide" data-x="center" data-y="['506', '390', '390', '310']"
              data-textAlign="['center']" data-frames='[{"delay":2200,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'>
              <?= l('contactar') ?>
            </a>
          </li>
          <li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-delay="6000">
            <!--  BACKGROUND IMAGE -->
            <img alt="Slide 2" src="[base_url]theme/theme/img/slide-02.jpg">
            <div class="tp-caption slide-title" data-x="center" data-y="['325', '210', '210', '180']" data-fontsize="['36', '36', '26', '24']"
              data-whitespace="normal" data-textAlign="['center']" data-frames='[{"delay":1600,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
              <?= l('Profesionals') ?>
            </div>
            <div class="tp-caption slide-content" data-x="center" data-y="center" data-width="['1170', '970', '768', '480']" data-fontsize="['72', '72', '62', '46']"
              data-textAlign="['center']" data-frames='[{"delay":1800,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'>
              <?= l('en formacions textils') ?>
            </div>
            <a href="[base_url]contacte.html" class="tp-caption au-btn au-btn--big au-btn--pill au-btn--yellow au-btn--slide" data-x="center" data-y="['506', '390', '390', '310']"
              data-textAlign="['center']" data-frames='[{"delay":2200,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'>
              <?= l('contactar') ?>
            </a>
          </li>
          <li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-delay="6000">
            <!--  BACKGROUND IMAGE -->
            <img alt="Slide 3" src="[base_url]theme/theme/img/slide-03.jpg">
            <div class="tp-caption slide-title" data-x="center" data-y="['325', '210', '210', '180']" data-fontsize="['36', '36', '26', '24']"
              data-whitespace="normal" data-textAlign="['center']" data-frames='[{"delay":1600,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
              <?= l('Profesionals') ?>
            </div>
            <div class="tp-caption slide-content" data-x="center" data-y="center" data-width="['1170', '970', '768', '480']" data-fontsize="['72', '72', '62', '46']"
              data-textAlign="['center']" data-frames='[{"delay":1800,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'>
              <?= l('innovació textil') ?>
            </div>
            <a href="[base_url]contacte.html" class="tp-caption au-btn au-btn--big au-btn--pill au-btn--yellow au-btn--slide" data-x="center" data-y="['506', '390', '390', '310']"
              data-textAlign="['center']" data-frames='[{"delay":2200,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'>
              <?= l('contactar') ?>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <!-- revolution slider end -->
  </section>
  <!-- End Slider -->
  <!-- Service -->
  <section class="service">
    <div class="service-wrap">
      <!-- end service intro -->
      <div class="service__item" style="background-image: url('[base_url]theme/theme/img/service-01.jpg');">
        <a href="<?= base_url() ?>projectes/internacionalitzacio" class="pro-link">
          <div class="service__item-inner">
            <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-112.png">
            <h4><?= l('internacionalitzacio') ?></h4>
          </div>
          <div class="pro-info">
            <div class="pro-title">
              <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-112.png">
            </div>
            <p class="pro-cat-name">
              <h4><?= l('internacionalitzacio') ?></h4>
            </p>
          </div>
        </a>
      </div>
      <!-- end service item -->
      <!-- end service intro -->
      <div class="service__item" style="background-image: url('[base_url]theme/theme/img/service-02.jpg');">
        <a href="<?= base_url() ?>projectes/innovacio" class="pro-link">
          <div class="service__item-inner">
            <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-01.png">
            <h4><?= l('innovacio') ?></h4>
          </div>
          <div class="pro-info">
            <div class="pro-title">
              <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-01.png">
            </div>
            <p class="pro-cat-name">
              <h4><?= l('innovacio') ?></h4>
            </p>
          </div>
        </a>
      </div>
      <!-- end service item -->
      <div class="service__item" style="background-image: url('[base_url]theme/theme/img/service-03.jpg');">
        <a href="[base_url]formacio.html" class="pro-link">
          <div class="service__item-inner">
            <img alt="Icon 2" src="[base_url]theme/theme/img/icon/icon-service-02.png">
            <h4><?= l('formacio') ?></h4>
          </div>
          <div class="pro-info">
            <div class="pro-title">
              <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-02.png">
            </div>
            <p class="pro-cat-name">
              <h4><?= l('formacio') ?></h4>
            </p>
          </div>
        </a>
      </div>
      <!-- end service item -->
      <div class="service__item" style="background-image: url('[base_url]theme/theme/img/service-04.jpg');">
        <a href="<?= base_url() ?>projectes/recerca-i-desenvolupament" class="pro-link">
          <div class="service__item-inner">
            <img alt="Icon 3" src="[base_url]theme/theme/img/icon/icon-service-03.png">
            <h4><?= l('Recerca i Desenvolupament') ?></h4>
          </div>
          <div class="pro-info">
            <div class="pro-title">
              <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-03.png">
            </div>
            <p class="pro-cat-name">
              <h4><?= l('Recerca i Desenvolupament') ?></h4>
            </p>
          </div>
        </a>
      </div>
      <!-- end service item -->
    </div>
  </section>
  <!-- End service -->
  <!-- Our Process -->
  <section class="our-process3">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="title title-3">
          <?= l('serveis') ?>
          </h2>
        </div>
        
      </div>
      <div class="our-process3-wrap clearfix js-slick-wrapper" data-slick-xs="1" data-slick-sm="1" data-slick-md="1" data-slick-lg="1" data-slick-xl="1"
        data-slick-dots="true" data-slick-appenddots=".dot-wrap" data-slick-thumb="true" data-slick-fade="true" data-slick-draggable="false">
        <div class="our-process3__inner js-slick-content" id="slick-pro-1">
          
          <?php foreach($this->elements->servicios(array('mostrar_en_main'=>1))->result() as $n=>$v): ?>
            <div class="our-process3__item clearfix" data-thumb="<?= $v->icono_main ?>">
              <div class="our-process3__text">
                <p class="num">
                  <?= ($n+1)<10?'0'.($n+1):($n+1) ?>
                </p>
                <p class="head">
                  <?= $v->nombre ?>
                </p>
                <p class="text">
                  <?= $v->descripcion_corta ?>
                </p>
                <p style="margin-top:30px">
                  <a href="<?= $v->url ?>" class="au-btn au-btn--small au-btn--pill au-btn--yellow au-btn--white"><?= l('Veure servei') ?></a>
                </p>
              </div>
              <div class="our-process3__big-img">
                <img src="<?= $v->foto_main ?>" alt="Process Image 1">
              </div>
            </div>
        <?php endforeach ?>

          
        </div>
        <div class="dot-wrap clearfix">
        </div>
      </div>
    </div>
  </section>
  <!-- End Our Process -->
  <!-- Statistic -->
  <section class="statistic">
    <div class="parallax parallax-statistic">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="statistic__item">
              <div class="number-wrap">
                <span class="number counterUp" data-counterup-time="350">244</span>+
              </div>
              <div class="label1">
                <?= l('Alumnes') ?>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="statistic__item">
              <div class="number-wrap">
                <span class="number counterUp" data-counterup-time="350">300</span>+
              </div>
              <div class="label1">
                <?= l('Empreses') ?>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="statistic__item">
              <div class="number-wrap">
                <span class="number counterUp" data-counterup-time="350">113</span>+
              </div>
              <div class="label1">
                <?= l('Hores lectives') ?>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="statistic__item">
              <div class="number-wrap">
                <span class="number counterUp" data-counterup-time="350">117</span>+
              </div>
              <div class="label1">
                <?= l('projectes') ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <!-- Blog -->
  <section class="blog">
    <div class="container">
      <!-- End Statistic -->
  
      <div class="row">
        <div class="col-md-12">
          <h2 class="title title-3">
          <?= l('últimes notícies') ?>
          </h2>
        </div>
      </div>
      <?php $blog = $this->elements->blog() ?>
  <?php if($blog->num_rows()>0): ?>
      <div class="row">        
        <?php if($blog->num_rows()>0): $b = $blog->row(); ?>
          <div class="col-lg-5 col-md-12">
            <div class="blog-single">
              <div class="img-blog">
                <a href="<?= $b->link ?>">
                  <img alt="Blog 1" src="<?= $b->foto_main ?>">
                  <div class="date date--big">
                    <div class="date__inner">
                      <span class="day"><?= $b->dia ?></span>
                      <span class="month"><?= $b->mes ?></span>
                    </div>
                  </div>
                </a>
              </div>
              <div class="blog-content">
                <h4 class="blog-title">
                <a href="<?= $b->link ?>"><?= $b->titulo ?></a>
                </h4>
                <p class="author">
                  <em>by <?= $b->user ?></em>
                </p>
                <p>
                  <?= cortar_palabras(strip_tags($b->texto),20) ?>
                </p>
              </div>
            </div>
          </div>
        <?php endif ?>
        <?php if($blog->num_rows()>1): ?>
          <div class="col-lg-7 col-md-12">
            
            <?php foreach($blog->result() as $n=>$b): if($n>0): ?>
              <div class="blog-item <?= $n>1?'m-t-50':'' ?>">
                <div class="row">
                  <div class="col-md-5 col-12">
                    <div class="img-blog">
                      <a href="<?= $b->link ?>">
                        <img alt="Blog 2" src="<?= $b->foto ?>">
                        <div class="date">
                          <div class="date__inner">
                            <span class="day"><?= $b->dia ?></span>
                            <span class="month"><?= $b->mes ?></span>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>

                  <div class="col-md-7 col-12">
                    <div class="blog-content">
                      <h4 class="blog-title">
                      <a href="<?= $b->link ?>">
                        <?= $b->titulo ?>
                      </a>
                      </h4>
                      <p class="author">
                        <em>by <?= $b->user ?></em>
                      </p>
                      <p>
                        <?= cortar_palabras(strip_tags($b->texto),20) ?>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; endforeach ?>
            
            <?php endif ?>


            <!-- End blog item   -->
            <div class="see-more">
              <a href="<?php echo base_url('blog') ?>" class="au-btn au-btn--big au-btn--pill au-btn--yellow au-btn--white">Veure més</a>
            </div>
          </div>
          <?php endif ?>
        </div>
          
      </div>

    </div>
  </section>
  <!-- End Blog -->

  <!-- Recent Project -->
  <section class="latest-project latest-project-4">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="title title-3">
          <?= l('Últimes formacions') ?>
          </h2>
        </div>
      </div>
    </div>
    <div class="parallax parallax--slide-re-pro">
      <div class="slide-re-pro owl-carousel owl-theme" id="owl-re-pro-2">
        
        <?php foreach($this->elements->cursos()->result() as $c): ?>
          <div class="slide-re-pro__item item">
            <img alt="Recent Project 1" src="<?= $c->foto_main ?>" class="img-responsive">
            <a href="<?= $c->link ?>" class="pro-link">
              <div class="pro-info">
                <h4 class="pro-title" style="font-size:26px">
                <?= $c->titulo ?>
                </h4>
                <p class="pro-cat-name" style="top:61%; color:#fff">
                  <?= $c->lugar ?>
                </p>
              </div>
            </a>
          </div>
        <?php endforeach ?>

      </div>
    </div>
  </section>
  <!-- End Recent Project -->
  <!-- Testi-Partner -->
  <!-- Testi-Partner -->
    <section class="testi-partner">
      <div class="container">
        <div class="row">
          
          <div class="col-lg-12 col-md-12 col-12">
            <div class="testi-partner__right">
              <h2 class="title title-3 title-3--left">
                <?= l('COL·LABORADORS') ?>
              </h2>
              <div class="partner-wrap1 owl-carousel owl-theme" id="owl-partner-2">
                <a href="" class="partner__item item">
                  <img alt="Partner 1" src="[base_url]theme/theme/img/partner-05.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 2" src="[base_url]theme/theme/img/partner-06.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 3" src="[base_url]theme/theme/img/partner-07.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 4" src="[base_url]theme/theme/img/partner-08.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 5" src="[base_url]theme/theme/img/partner-08.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 6" src="[base_url]theme/theme/img/partner-07.png">
                </a>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  <!-- End Testi-Partner -->
  <?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
[footer]
[scripts]