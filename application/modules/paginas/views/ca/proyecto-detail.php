<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>

<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--ser-li">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= $detail->titulo ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]"><?= l('inici') ?></a>
                </li>
                <span>/</span>
                <li>
                  <a href="<?= base_url('projectes/'.toUrl($detail->categoria->nombre)) ?>"><?= $detail->categoria->nombre ?></a>
                </li>
                <span>/</span>
                <li class="active">
                 <?= $detail->titulo ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- Service Content -->
    <section class="service-content p-t-50">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-4">
            <div class="service-tab">
              <ul class="ul--no-style">
                <?php foreach($this->elements->get_proyectos(array('categoria_proyectos.id'=>$detail->categoria->id))->row()->proyectos->result() as $p): ?>
					<li <?= $p->id==$detail->id?' class="active"':'' ?>>
						<a href="<?= $p->url ?>">
							<?= cortar_palabras($p->titulo,3) ?>
						</a>
					</li>
				<?php endforeach ?>
              </ul>              
            </div>
          </div>
          <div class="col-lg-9 col-md-8">
            <div class="service-text m-t-50">
              <h5>
                <?= $detail->titulo ?>
              </h5>
              <p class="m-b-35">
                <?= $detail->descripcion ?>
              </p>
              <div  class="m-t-40">
                <img alt="Service Big" src="<?= $detail->foto ?>">
              </div>
              <div class="service-img-wrap">
                <div class="service-img">
                  <a href="<?= base_url() ?>theme/theme/img/service-13.jpg" data-lightbox="service2">
                    <img alt="Service Thumb 1" src="<?= base_url() ?>theme/theme/img/service-13.jpg">
                  </a>
                </div>
                <div class="service-img">
                  <a href="<?= base_url() ?>theme/theme/img/service-14.jpg" data-lightbox="service2">
                    <img alt="Service Thumb 2" src="<?= base_url() ?>theme/theme/img/service-14.jpg">
                  </a>
                </div>
                <div class="service-img">
                  <a href="<?= base_url() ?>theme/theme/img/service-15.jpg" data-lightbox="service2">
                    <img alt="Service Thumb 3" src="<?= base_url() ?>theme/theme/img/service-15.jpg">
                  </a>
                </div>
                <div class="service-img">
                  <a href="<?= base_url() ?>theme/theme/img/service-16.jpg" data-lightbox="service2">
                    <img alt="Service Thumb 4" src="<?= base_url() ?>theme/theme/img/service-16.jpg">
                  </a>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Service Content -->
    <section class="service-content">
  <div class="contact2">
        <div class="container ">
          <div class="row ">
            <div class="col-lg-3 col-md-4 ">
            
              
            </div>
            <div class="col-lg-9 col-md-8 ">
            <h5 class="title title-3 title-3--right">
                <?= l('COL·LABORADORS') ?>
              </h5>
              <div class="partner-wrap1 partner-wrap2 owl-carousel owl-theme" id="owl-partner-2">
                <a href="" class="partner__item item">
                  <img alt="Partner 1" src="<?= base_url() ?>theme/theme/img/partner-05.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 2" src="<?= base_url() ?>theme/theme/img/partner-06.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 3" src="<?= base_url() ?>theme/theme/img/partner-07.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 4" src="<?= base_url() ?>theme/theme/img/partner-08.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 5" src="<?= base_url() ?>theme/theme/img/partner-06.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 6" src="<?= base_url() ?>theme/theme/img/partner-07.png">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      </section>

<?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>