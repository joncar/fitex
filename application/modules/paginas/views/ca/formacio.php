[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--ser-li"  style="background-image: url('[base_url]theme/theme/img/bg-head-formacio.jpg');">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= l('formacio') ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]"><?= l('inici') ?></a>
                </li>
                <span>/</span>
                <li>
                  <?= l('qui som') ?>
                </li>
                <span>/</span>
                <li class="active">
                 <?= l('formacio') ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="service-2" style="padding-top:80px;">
      <div class="container clearfix">
      	<h2 class="title title-3 title-3--center"  style="margin-bottom: 180px;">
          <?= l('formacio') ?>
        </h2>
        <div class="service-2-wrap clearfix">
          <div class="service-2__left wow fadeInLeft" data-wow-duration="1s">
            <div class="service-2__img">
              <img alt="Service thumb" src="[base_url]theme/theme/img/service-page2.jpg" style="margin-right: -60px;margin-left: 2px;">
              <img alt="Service thumb" class="d-none d-md-block" src="[base_url]theme/theme/img/service-page33.jpg" style="margin-top:80px;margin-right: -60px;margin-left: 80px;">
            </div>
          </div>



          <div class="service-2__right service-3__right">            
            <div class="service-2__inner-body formacion__inner-body wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
              
              
              <div class="row">

                  <?php foreach($this->elements->formaciones_areas()->result() as $f): ?>
                    
                      <div class="col-md-6 box-item body__item">
                        
                          <div class="box-head box-head--border">
                            <img alt="Icon 1" src="<?= $f->icono ?>">
                          </div>
                          <div class="box-body"> 
                            <a href="<?= base_url().'formacio/'.$f->url ?>">
                              <h5><?= $f->titulo ?></h5>
                              <p>
                                <?= $f->descripcion ?>
                              </p>
                            </a>
                            <br/>  
                              <?php if($f->cursos->num_rows()>0): ?>
                                <a href="<?= $f->link ?>" class="au-btn au-btn--pill au-btn--yellow au-btn--white">Veure cursos</a>
                              <?php endif ?>
                          </div>
                        
                      </div>
                    
                  <?php endforeach ?>


              </div>


            </div>



          </div>
        </div>
      </div>
    </section>
    <!-- End Service 2 -->

    <!-- Recent News -->
    <section class="recent-news" style=" padding: 30px;">
      <div class="container">
        <h2 class="title formacio title-3 title-3--center">
           <?= l('BONIFICACIONS FUNDAE') ?>
        </h2>
        <div class="row">
          
          <div class="col-xs-12 col-md-8">            
            <p class="m-b-75 title-detail--px-45">
                <?= l('fundae-text') ?>
            </p>
          </div>
          <div class="col-xs-12 col-md-4">
            <img alt="Service thumb" class="d-none d-md-block" src="<?= base_url() ?>theme/theme/img/service-page3.jpg">
          </div>
        </div>

      <?php $this->load->view($this->theme.'_contacte_bar_2'); ?>
    </section>
    <!-- End Service List -->
[footer]
[scripts]