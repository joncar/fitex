[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--nav-1">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= l('avis legal') ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]">Fitex</a>
                </li>
                <span>/</span>
                <li class="active">
                  <?= l('avis legal') ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- We Are -->
    <section class="we-are">
      <div class="container">
        <div class="row no-gutters">          
          <div class="col-lg-12 col-md-12 col-12">
            <div class="we-are__right">
              <h2></h2>
              <h2 class="title--small"></h2>

              <h5></h5>              	
				      <?= l('avis-legal-text') ?>
            </div>
          </div>
        </div>
      </div>
    </section>
 <?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
    [footer]
    [scripts]