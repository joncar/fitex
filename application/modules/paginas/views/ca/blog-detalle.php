[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--nav-3">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= l('noticies') ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="<?= base_url() ?>"><?= l('inici') ?></a>
                </li>
                <span>/</span>
                <li>
                  <a href=""><?= l('noticies') ?></a>
                </li>
                <span>/</span>
                <li class="active">
                  <?= l('noticies') ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- Blog Detail -->
    <section class="blog-detail">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-7">
            <div class="blog-thumb">
              <img alt="Blog 1" src="<?= $detail->foto ?>">
            </div>
            <h4 class="blog-title">
              <?= $detail->titulo ?>
            </h4>
            <p class="blog-meta">
              <em class="author">Per <?= $detail->user ?></em>
              <em class="cate">Design Knowledge</em>
              <em class="time"><?= $detail->fecha ?></em>
            </p>
            <div class="blog-content">
              <?= $detail->texto ?>
            </div>
            <div class="blog-footer">
              <div class="container clearfix">
                <div class="tags pull-left m-l-20">
                  <span>Tags: </span>
                  <?php foreach(explode(',',$detail->tags) as $t): ?>
                    <a href="#"><?= $t ?></a>,
                  <?php endforeach ?>                  
                </div>
                <div class="share pull-right">
                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('blog/'.toUrl($detail->id.'-'.$detail->titulo)) ?>">
                    <i class="zmdi zmdi-facebook"></i>
                  </a>
                  <a href="https://twitter.com/share?text=<?= urlencode($detail->titulo) ?>&url=<?= base_url('blog/'.toUrl($detail->id.'-'.$detail->titulo)) ?>">
                    <i class="zmdi zmdi-twitter"></i>
                  </a>
                </div>
              </div>
            </div>
            
            
          </div>
          <div class="col-lg-4 col-md-5">
            <div class="blog-sidebar">
              <div class="blog__search">
                <form action="<?php echo site_url('blog') ?>" method="get" role="search" class="search-form">
                  <input type="text" name="direccion" placeholder="Buscar">
                  <button type="submit">
                    <i class="fa fa-search"></i>
                  </button>
                </form>
              </div>
              <div class="blog__recent">
                <h4 class="title-sidebar">
                  Relacionados
                </h4>
                
                <?php foreach($relacionados->result() as $r): ?>
                  <div class="blog__recent-item clearfix">
                    <div class="img pull-left">
                      <a href="<?= $r->link ?>">
                        <img alt="Blog Thumb 1" src="<?= $r->foto ?>">
                      </a>
                    </div>
                    <div class="text">
                      <h6>
                        <a href="<?= $r->link ?>">
                          <?= $r->titulo ?>
                        </a>
                      </h6>
                      <p>
                        <em><?= $r->fecha ?></em>
                      </p>
                    </div>
                  </div>
                  <!-- End Blog recent item -->
                <?php endforeach ?>
                <!-- End Blog recent item -->
              </div>
              <ul class="blog__cate ul--no-style">
                <h4 class="title-sidebar">
                  Categorias
                </h4>
                
                <?php foreach($categorias->result() as $c): ?>
                <li>
                  <a href="<?= base_url('blog') ?>?blog_Categorias_id=<?= $c->id ?>">
                    <?= $c->blog_categorias_nombre ?>                    
                  </a>
                </li>
                <?php endforeach ?>
              </ul>
              <div class="blog__tag-wrap">
                <h4 class="title-sidebar">
                  tags
                </h4>
                <div class="blog__tag">
                  <?php foreach(explode(',',$detail->tags) as $t): ?>
                    <a href="#"><?= $t ?></a>,
                  <?php endforeach ?>  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Blog Detail -->
<?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
[footer]
[scripts]