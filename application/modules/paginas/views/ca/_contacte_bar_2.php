<!-- Contact 2 -->
      <div class="contact2">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 col-md-12">
              <div class="contact2__item">
                <p>
                  <?= l('tacompanyem en el teu projecte') ?>
                </p>
                <div>
                  <a href="contacte.html" class="au-btn au-btn--big au-btn--pill au-btn--yellow au-btn--white"><?= l('Contactans') ?></a>
                </div>
              </div>
            </div>
            <div class="col-lg-8 col-md-12">
              <div class="partner-wrap1 owl-carousel owl-theme" id="owl-partner-2">
                <a href="" class="partner__item item">
                  <img alt="Partner 1" src="[base_url]theme/theme/img/partner-05.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 2" src="[base_url]theme/theme/img/partner-06.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 3" src="[base_url]theme/theme/img/partner-07.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 4" src="[base_url]theme/theme/img/partner-08.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 5" src="[base_url]theme/theme/img/partner-06.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 6" src="[base_url]theme/theme/img/partner-07.png">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Contact2 -->