[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--nav-2">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= l('Contacte') ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]"><?= l('inici') ?></a>
                </li>
                <span>/</span>
                <li class="active">
                  <?= l('Contacte') ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->


    <!-- Contact content -->
    <section id="contact" class="contact">
      <div class="container">
        <div class="m-t-40">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 widgets-contact mb-60-xs">
              <div class="form-contact-wrap m-t-40">
                <div class="heading heading-4">
                  <div class="heading-bg heading-right">                  
                     <h4><?= l('Informació de Contacte') ?></h4>
                  </div>
                </div>
                <div class="widget">
                  <div class="widget-contact-icon pull-left">
                    <i class="lnr lnr-store"></i>
                  </div>
                  <div class="widget-contact-info">
                    <p class=""><?= l('Visitans') ?></p>
                    <p class="text-capitalize font-heading">
                       <?= l('Av. Mestre Muntaner, 86 08700 Igualada') ?>
                    </p>
                  </div>
                </div>
                <!-- .widget end -->
                <div class="clearfix">
                </div>
                <div class="widget">
                  <div class="widget-contact-icon pull-left">
                    <i class="lnr lnr-envelope"></i>
                  </div>
                  <div class="widget-contact-info">
                    <p class="text-capitalize ">Email</p>
                    <p class="font-heading">
                      <a href="mailto:general@fitex.es">
                        general@fitex.es
                      </a>
                    </p>
                  </div>
                </div>
                <!-- .widget end -->
                <div class="clearfix">
                </div>
                <div class="widget">
                  <div class="widget-contact-icon pull-left" style="margin-bottom: 20px;">
                    <i class="lnr lnr-phone"></i>
                  </div>
                  <div class="widget-contact-info">
                    <p class="text-initial"><?= l('Trucans al') ?></p>
                    <p class="text-capitalize font-heading">
                      <a href="tel:34938035762">+34 938 03 57 62 </a>
                    </p>
                  </div>
                </div>
                <!-- .widget end -->
              </div>
            </div>
            <!-- .col-md-4 end -->
            <div class="col-xs-12 col-sm-12 col-md-8">
              <div class="form-contact-wrap m-t-40">
                  <h4><?= l('Formulari de Contacte') ?></h4>
                  <form class="form-contact" role="form" action="paginas/frontend/contacto" onsubmit="sendForm(this,'.response'); return false;">
                    <div class="row">
                      <div class="col-md-4 col-12">
                        <input type="text" name="nombre" placeholder="<?= l('El teu nom*') ?>">
                      </div>
                      <div class="col-md-4 col-12">
                        <input type="email" name="email"  placeholder="<?= l('El teu Email*') ?>">
                      </div>
                      <div class="col-md-4 col-12">
                        <input type="phone" name="extras[telefono]"  placeholder="<?= l('El teu telèfon*') ?>">
                      </div>
                      <div class="col-md-12">
                        <textarea name="extras[message]" class="message" placeholder="<?= l('Missatge') ?>"></textarea>
                      </div>
                      <div class="col-md-12 m-t-10"> 
                        <div class="form-group form-check">
                          <input type="hidden" name="titulo" value="Solicitud de contacto">
                          <input type="checkbox" value="1" name="politicas" style="width:auto"> 
                          <label for="politicas"><?= l('He llegit i accepto la ') ?><a href="<?= base_url() ?>avis-legal.html" style="text-decoration: underline;"><?= l('política de privacitat') ?></a> </label>
                        </div>                     
                      </div>
                    </div>
                    <div class="response"></div>
                    <div>
                      <button type="submit" class="au-btn au-btn--pill au-btn--yellow au-btn--big"><?= l('Enviar') ?></button>
                    </div>
                  </form>
              </div>
            </div>
            <!-- .col-md-8 end -->
          </div>
          <!-- .row end -->
          <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
      </div>
      <!-- .container end -->
    </section>


    
    <section class="m-t-40">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11937.195039489006!2d1.627896!3d41.5844191!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe598a158be9cfbb7!2sFitex!5e0!3m2!1ses!2ses!4v1542357986142" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </section>
    <!-- End Contact Content -->
    
    <!-- End Contact Info -->
    [footer]
    <script src="https://www.google.com/recaptcha/api.js?render=6Lc1z7cZAAAAACj_O8Jd92T4OhF-CwPMB6hxTnLa"></script>
    [scripts]