[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--nav-1">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                Avís legal
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]">Fitex</a>
                </li>
                <span>/</span>
                <li class="active">
                  Avís legal
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- We Are -->
    <section class="we-are">
      <div class="container">
        <div class="row no-gutters">          
          <div class="col-lg-12 col-md-12 col-12">
            <div class="we-are__right">
              <h2>Aviso Legal</h2>
              <h2 class="title--small"></h2>

              <h5></h5>              	
				<p><strong>CONSENTIMIENTO DEL INTERESADO.</strong> Para todas las finalidades que han sido especificadas y tipificadas en la secci&oacute;n anterior. <strong>INTER&Eacute;S LEG&Iacute;TIMO.</strong> Para la finalidad de publicidad y prospecci&oacute;n comercial. Se refiere en exclusiva al tratamiento o procesamiento de datos para la prospecci&oacute;n convencional y otras formas de comercializaci&oacute;n o publicidad, lo que incluye la mercadotecnia directa o marketing directo. De acuerdo con el <strong>Considerado 47 del Reglamento (UE) n&uacute;m.2016/679</strong>, General de Protecci&oacute;n de Datos el inter&eacute;s leg&iacute;timo podr&iacute;a darse "cuando existe una relaci&oacute;n pertinente y apropiada entre el interesado y el responsable, como en situaciones en las que el interesado es cliente o est&aacute; al servicio del responsable", como ocurre precisamente en el caso que nos ocupa. A&ntilde;ade, <strong>el considerando 47 del RGPD</strong>, que "El tratamiento de datos personales con fines de mercadotecnia directa puede considerarse realizado por inter&eacute;s leg&iacute;timo".</p>
				<p>&nbsp;</p>
				<p>Adem&aacute;s, <strong>el Dictamen del Grupo de Trabajo del Art&iacute;culo 29 (GT29)</strong> n&uacute;m. 06/2014, de 9 de abril de 2014, sobre el concepto de inter&eacute;s leg&iacute;timo del responsable del tratamiento de los datos, incluye como posible o eventual "inter&eacute;s leg&iacute;timo" tanto "la prospecci&oacute;n convencional y otras formas de comercializaci&oacute;n o publicidad" como "El tratamiento con fines de investigaci&oacute;n (incluida la investigaci&oacute;n de mercado)". En el caso del Responsable del Tratamiento identificado en el presente Fichero de Actividad, el inter&eacute;s leg&iacute;timo se justifica por tratarse de un supuesto muy concreto en el que el tratamiento de datos es necesario e ineludible para la finalidad de publicidad y prospecci&oacute;n comercial, que se podr&aacute; desarrollar de forma correcta gracias al uso de t&eacute;cnicas de marketing directo que son apropiadas e id&oacute;neas, y que no suponen, en ning&uacute;n caso, un riesgo objetivo elevado para los derechos de los interesados. En este caso, el Responsable del Tratamiento no dispone de otros medios para satisfacer el inter&eacute;s leg&iacute;timo que sean menos invasivos. En definitiva, se trata de un inter&eacute;s leg&iacute;timo l&iacute;cito de acuerdo con la literalidad del <strong>considerando 47 del RGPD</strong> y la interpretaci&oacute;n que hasta la fecha ha realizado el GT29, suficientemente concreto y representativo de un inter&eacute;s real y actual.</p>
				<p>&nbsp;</p>
				<p><strong>Base de legitimaci&oacute;n para el tratamiento de datos</strong><br /><strong></strong></p>
				<p><span style="font-size: 10pt;"><strong>EJECUCI&Oacute;N DE UN CONTRATO.</strong></span> En el marco de la finalidad de comercio electr&oacute;nico, gestionar los datos del cliente a fin de proceder al cobro en el marco de una compraventa en l&iacute;nea. Se trata de relaci&oacute;n contractual a la que se aplican las normas de derecho privado, y en el caso que sea de aplicaci&oacute;n, tambi&eacute;n la normativa de consumidores y usuarios</p>
            </div>
          </div>
        </div>
      </div>
    </section>
 <?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
    [footer]
    [scripts]