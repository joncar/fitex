[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--nav-3">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= l('noticies') ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]"><?= l('inici') ?></a>
                </li>
                <span>/</span>
                <li class="active">
                  <?= l('noticies') ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- Blog Grid 2 -->
    <div class="blog1 blog2">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div id="filter-wrap">
              <ul id="filter" class="ul--no-style ul--inline">
                
                <li class="active">
                  <span data-filter="*"><?= l('Totes') ?></span>
                </li>
                <?php foreach($categorias->result() as $c): ?>
                  <li>
                    <span data-filter=".cat<?= $c->id ?>"><?= $c->blog_categorias_nombre ?></span>
                  </li>
                <?php endforeach ?>

              </ul>
            </div>
          </div>
        </div>
        <div id="isotope-grid" class="clearfix">
          
            <?php foreach($detail->result() as $b): ?>
              <div class="col-lg-3 col-md-6 item cat<?= $b->blog_categorias_id ?>">
                <div class="blog-item">
                  <div class="img-blog">
                    <a href="<?= $b->link ?>">
                      <img alt="Blog 1" src="<?= $b->foto ?>">
                    </a>
                  </div>
                  <div class="blog-content">
                    <h4 class="blog-title">
                      <a href="<?= $b->link ?>"><?= $b->titulo ?></a>
                    </h4>
                    <p class="blog-meta">
                      <em class="author">Per <?= $b->user ?></em>
                      <em class="cate">Design Knowledge</em>
                      <em class="time"><?= $b->fecha ?></em>
                    </p>
                    <p>
                      <?= strip_tags($b->texto) ?>
                    </p>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
          
        </div>
      </div>
    </div>
    <!-- End Blog Grid 2 -->
<?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
    [footer]
    [scripts]