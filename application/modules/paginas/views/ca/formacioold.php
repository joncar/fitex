[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--ser-li"  style="background-image: url('[base_url]theme/theme/img/bg-head-formacio.jpg');">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                Formació
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]">Inici</a>
                </li>
                <span>/</span>
                <li>
                  Qui Som
                </li>
                <span>/</span>
                <li class="active">
                 Formació
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- 
End Navigation    <section class="service-content p-t-50">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-4">
            <?php $this->load->view($this->theme.'_qui_som',array('active'=>3)); ?>            
          </div>
          <div class="col-lg-9 col-md-8">
            <div class="service-text m-t-50">
              <h5>
                FORMACIÓ
              </h5>
              <p class="m-b-35">
                En col·laboració amb el centre tecnològic FITEX, disposem d’un catàleg d’accions formatives de professionalització, destinades a professionals del sector tèxtil-moda (formació contínua, formació per a l’ocupació i formació específica per a empreses) per àrees:

              </p>                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
 -->
      <!-- Service 2 -->
    <section class="service-2" style="padding-top:80px;">
      <div class="container clearfix">
      	<h2 class="title title-3 title-3--center"  style="margin-bottom: 180px;">
          Formació
        </h2>
        <div class="service-2-wrap clearfix">
          <div class="service-2__left wow fadeInLeft" data-wow-duration="1s">
            <div class="service-2__img">
              <!--<h2 class="title title-2--special title-small" id="our1">
                Our
              </h2>
              <h2 class="title title-2--special title-small" id="our2">
                Services
              </h2>-->
              <img alt="Service thumb" src="[base_url]theme/theme/img/service-page2.jpg">
              <img alt="Service thumb" class="d-none d-md-block" src="[base_url]theme/theme/img/service-page3.jpg" style="margin-top:80px;margin-right: -60px;margin-left: 80px;">
            </div>
          </div>



          <div class="service-2__right service-3__right">            



            <div class="service-2__inner-body formacion__inner-body wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
              <div class="row">
                  <div class="col-md-6 box-item body__item clearfix">
                    <div class="box-head box-head--border">
                      <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-04.png">
                    </div>
                    <div class="box-body">                      
                        <h5>ÀREA TÈCNICA I TECNOLÒGICA</h5>
                        <p>
                          Àrea orientada a professionals de la producció per interaccionar sobre la creació de producte.
                        </p>                      
                    </div>
                  </div>
                  <div class="col-md-6 box-item body__item clearfix">
                    <div class="box-head box-head--border">
                      <img alt="Icon 2" src="[base_url]theme/theme/img/icon/icon-service-05.png">
                    </div>
                    <div class="box-body">
                      <h5>ÀREA D’ESTRATÈGIA I INNOVACIÓ</h5>
                      <p>
                        Àrea orientada a professionals de qualsevol àrea funcional de l’empresa, de cultura innovadora.
                      </p>
                    </div>
                  </div>
              </div>
              

              <div class="row">
                  <div class="col-md-6 box-item body__item clearfix">
                    <div class="box-head box-head--border">
                      <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-06.png">
                    </div>
                    <div class="box-body">
                      <h5>ÀREA D’INTERNACIONALITZACIÓ</h5>
                      <p>
                        Àrea orientada a professionals d’exportació i d'administració de les empreses del sector tèxtil-moda.
                      </p>
                    </div>
                  </div>
                  <div class="col-md-6 box-item body__item clearfix">
                    <div class="box-head box-head--border">
                      <img alt="Icon 2" src="[base_url]theme/theme/img/icon/icon-service-07.png">
                    </div>
                    <div class="box-body">
                      <h5>ÀREA DE QUALITAT I GESTIÓ</h5>
                      <p>
                        Fabricar productes capaços de satisfer unes necessitats, respectuosos amb el medi...
                      </p>
                    </div>
                  </div>
              </div>

              <div class="row">
                  <div class="col-md-6 box-item body__item clearfix">
                    <div class="box-head box-head--border">
                      <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-08.png">
                    </div>
                    <div class="box-body">
                      <h5>ÀREA <br>TIC’s</h5>
                      <p>
                        Eines de gestió que afecten a diferents àrees funcionals de l’empresa.
                      </p>
                    </div>
                  </div>
                  <div class="col-md-6 box-item body__item clearfix">
                    <div class="box-head box-head--border">
                      <img alt="Icon 2" src="[base_url]theme/theme/img/icon/icon-service-09.png">
                    </div>
                    <div class="box-body">
                      <h5>ÀREA DE GESTIÓ DE RRHH</h5>
                      <p>
                        Orientada a la capacitació de professionals per a la gestió de persones i equips de treball.
                      </p>
                    </div>
                  </div>
              </div>

              <div class="row">
                  <div class="col-md-6 box-item body__item clearfix">
                    <div class="box-head box-head--border">
                      <img alt="Icon 1" src="[base_url]theme/theme/img/icon/icon-service-10.png">
                    </div>
                    <div class="box-body">
                      <h5>ÀREA RETAIL <br>I BRANDING</h5>
                      <p>
                        Àrea de transferència de coneixements en el món de la moda, estudis de mercat i de consum...
                      </p>
                    </div>
                  </div>
                  <div class="col-md-6 box-item body__item clearfix">
                    <div class="box-head box-head--border">
                      <img alt="Icon 2" src="[base_url]theme/theme/img/icon/icon-service-11.png">
                    </div>
                    <div class="box-body">
                      <h5>ÀREA COMERCIAL AMB IDIOMES</h5>
                      <p>
                        El comerç lliure fa que els treballadors hagin de dominar varis idiomes en tots els departaments.
                      </p>
                    </div>
                  </div>
              </div>
            </div>



          </div>
        </div>
      </div>
    </section>
    <!-- End Service 2 -->

    <!-- Recent News -->
    <section class="recent-news" style=" padding: 30px;">
      <div class="container">
        <div class="row justify-content-center">
          <div >
            <h2 class="title formacio title-3 title-3--center">
               BONIFICACIONS FUNDAE
              </h2>
            <p class="title-detail m-b-75 title-detail--px-45">
              Ajudem i assessorem a gestionar la formació que realitzen els treballadors de la vostra empresa a través de la FUNDAE (Fundación Estatal para la Formación en el Empleo) per a la seva bonificació, així com també organitzem formació a mida per a les empreses.
<br>El Centre FITEX està registrat com a entitat de formació per a impartir formació no inclosa en el catàleg d’especialitats formatives en les següents àrees:
<br>• Logística comercial i gestió del transport
<br>• Màrqueting i relacions públiques
<br>• Confecció en tèxtil i pell
<br>• Ennobliment de matèries tèxtils i pell
<br>• Producció de fils i teixits
<br>PER A MÉS INFORMACIÓ: angels@fitex.es
            </p>
          </div>
        </div>
        <!-- 
<div class="row">
          <div class="col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="img-blog m-b-40 m-t-20">
              <a href="blog-detail.html">
                <img alt="Blog 1" src="[base_url]theme/theme/img/blog-04.jpg">
                <div class="date date~~big">
                  <div class="date__inner">
                    <span class="day">15</span>
                    <span class="month">feb</span>
                  </div>
                </div>
              </a>
            </div>
            <div class="blog-content">
              <h4 class="blog-title m-b-10">
                <a href="blog-detail.html">morden interior</a>
              </h4>
              <div class="blog-link clearfix m-b-8">
                <div class="blog-link__item">
                  <span>
                    15
                  </span>
                  <a href="">
                    <i class="fa fa-commenting-o"></i>
                  </a>
                </div>
                <div class="blog-link__item">
                  <span>
                    12
                  </span>
                  <a href="">
                    <i class="fa fa-thumbs-o-up"></i>
                  </a>
                </div>
                <div class="blog-link__item">
                  <span>
                    10
                  </span>
                  <a href="">
                    <i class="fa fa-share"></i>
                  </a>
                </div>
              </div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidi utonia labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation ullamcod laboris nisi ut aliquip ex ea commodo consequat
              </p>
            </div>
            <div class="see-more see-more~~left">
              <a href="blog-detail.html" class="au-btn au-btn~~big au-btn~~pill au-btn~~yellow au-btn~~white">See More</a>
            </div>
          </div>
          <div class="col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="img-blog m-b-40 m-t-20">
              <a href="blog-detail.html">
                <img alt="Blog 2" src="[base_url]theme/theme/img/blog-05.jpg">
                <div class="date date~~big">
                  <div class="date__inner">
                    <span class="day">24</span>
                    <span class="month">feb</span>
                  </div>
                </div>
              </a>
            </div>
            <div class="blog-content">
              <h4 class="blog-title m-b-10">
                <a href="blog-detail.html">form Ideal to master piece</a>
              </h4>
              <div class="blog-link clearfix m-b-8">
                <div class="blog-link__item">
                  <span>
                    15
                  </span>
                  <a href="">
                    <i class="fa fa-commenting-o"></i>
                  </a>
                </div>
                <div class="blog-link__item">
                  <span>
                    12
                  </span>
                  <a href="">
                    <i class="fa fa-thumbs-o-up"></i>
                  </a>
                </div>
                <div class="blog-link__item">
                  <span>
                    10
                  </span>
                  <a href="">
                    <i class="fa fa-share"></i>
                  </a>
                </div>
              </div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidi utonia labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation ullamcod laboris nisi ut aliquip ex ea commodo consequat
              </p>
            </div>
            <div class="see-more see-more~~left">
              <a href="blog-detail.html" class="au-btn au-btn~~big au-btn~~pill au-btn~~yellow au-btn~~white">See More</a>
            </div>
          </div>
        </div>
      </div>
    </section>
 -->
    <!-- End Recent News -->

      <?php $this->load->view($this->theme.'_contacte_bar_2'); ?>
    </section>
    <!-- End Service List -->
[footer]
[scripts]