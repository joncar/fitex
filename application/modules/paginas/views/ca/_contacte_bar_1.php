<!-- Contact -->
  <section class="contact">
    <div class="parallax parallax--contact">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="contact__inner clearfix">
              <p class="contact__content">
                <?= l('tacompanyem en el teu projecte') ?>
              </p>
              <a href="[base_url]contacte.html" class="au-btn au-btn--big au-btn--pill au-btn--dark"><?= l('Contactans') ?></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Contact -->