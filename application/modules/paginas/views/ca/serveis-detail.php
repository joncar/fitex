[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--ser-li">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= $d->nombre ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]"><?= l('inici') ?></a>
                </li>
                <span>/</span>
                <li>
                  <a href="[base_url]serveis.html"><?= l('serveis') ?></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- Service Content -->
    <section class="service-content p-t-50">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-4">
            <div class="service-tab">
              <ul class="ul--no-style">
                
                <?php foreach($this->elements->servicios()->result() as $s): ?>
                  <li class="<?= $s->id==$d->id?'active':'' ?>">
                    <a href="<?= $s->url ?>">
                      <?= $s->nombre ?>
                    </a>
                  </li>
                <?php endforeach ?>


              </ul>
              <div class="contact2__item">
                <p>
                  <?= l('Tacompanyem en el teu projecte') ?>
                </p>
                <div>
                  <a href="<?= base_url() ?>contacte.html" class="au-btn au-btn--pill au-btn--yellow au-btn--medium">Contacta'ns</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-9 col-md-8">
            <div class="service-text m-t-50">
              <div class="m-b-35">
                <?= $d->descripcion ?>
              </div>
              <div>
                <img alt="Service Big" src="<?= $d->fotos[0] ?>">
              </div>
              <div class="service-img-wrap">
                
                <?php foreach($d->fotos as $f): if(!empty($f)): ?>
                  <div class="service-img">
                    <a href="<?= $f ?>" data-lightbox="service2">
                      <img alt="Service Thumb 1" src="<?= $f ?>">
                    </a>
                  </div>
                <?php endif; endforeach ?>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Service Content -->
    <section class="service-content">
  <div class="contact2">
        <div class="container ">
          <div class="row ">
            <div class="col-lg-3 col-md-4 ">
            
              
            </div>
            <div class="col-lg-9 col-md-8 ">
            <h5 class="title title-3 title-3--right">
                <?= l('COL·LABORADORS') ?>
              </h5>
              <div class="partner-wrap1 partner-wrap2 owl-carousel owl-theme" id="owl-partner-2">
                <a href="" class="partner__item item">
                  <img alt="Partner 1" src="[base_url]theme/theme/img/partner-05.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 2" src="[base_url]theme/theme/img/partner-06.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 3" src="[base_url]theme/theme/img/partner-07.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 4" src="[base_url]theme/theme/img/partner-08.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 5" src="[base_url]theme/theme/img/partner-06.png">
                </a>
                <a href="" class="partner__item item">
                  <img alt="Partner 6" src="[base_url]theme/theme/img/partner-07.png">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      </section>
      <?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
[footer]
[scripts]