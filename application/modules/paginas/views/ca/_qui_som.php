<div class="service-tab">
	<ul class="ul--no-style">
		<li <?= $active==1?' class="active"':'' ?>>
			<a href="[base_url]internacionalitzacio.html">
				<?= l('internacionalitzacio') ?>
			</a>
		</li>
		<li <?= $active==2?' class="active"':'' ?>>
			<a href="[base_url]innovacio.html">
				<?= l('innovacio') ?>
			</a>
		</li>
		<li <?= $active==3?' class="active"':'' ?>>
			<a href="[base_url]formacio.html">
				<?= l('formacio') ?>
			</a>
		</li>
		<li <?= $active==4?' class="active"':'' ?>>
			<a href="[base_url]recerca-i-desenvolupament.html">
				<?= l('Recerca i Desenvolupament') ?>
			</a>
		</li>
		
	</ul>
</div>