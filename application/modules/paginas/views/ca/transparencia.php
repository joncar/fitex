[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--ser-li" style="background:url('<?= base_url() ?>theme/theme/img/servei.jpg')">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                Portal de Transparència
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]">Inici</a>
                </li>
                <span>/</span>                
                <li class="active">
                  Portal de Transparència
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- Service List -->
    <section class="service-list">
      <div class="container">
        <div class="row">
          
        <?php foreach($this->elements->comte_anuals()->result() as $s): ?>
          <div class="col-lg-4 col-md-6">
            <div class="service-list__item img-blog">
              <a href="<?= $s->fichero ?>" target="_new">
                <img alt="Service 1" src="<?= $s->foto ?>">
              </a>
              <div class="service-list__text">
                <h5>
                  <a href="<?= $s->fichero ?>" target="_new"><?= $s->nombre ?></a>
                </h5>
                <p>
                  <?= $s->descripcion ?>                  
                </p>
                <br/>
                <a href="<?= $s->fichero ?>" target="_new" class="au-btn au-btn--pill au-btn--yellow au-btn--white">Veure</a>
              </div>
            </div>
          </div>
        <?php endforeach ?>
          

        </div>
      </div>
      <?php $this->load->view($this->theme.'_contacte_bar_2'); ?>
    </section>
    <!-- End Service List -->
[footer]
[scripts]