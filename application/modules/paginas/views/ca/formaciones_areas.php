<?php $this->load->view($this->theme.'menu',array(),FALSE,'paginas'); ?>

<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--ser-li">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= $detail->titulo ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]"><?= l('inici') ?></a>
                </li>
                <span>/</span>
                <li>
                  <a href="<?= base_url('formacio.html') ?>"><?= l('formacio') ?></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- Service Content -->
<section class="service-content p-t-50">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-4">
        <div class="service-tab">
          <ul class="ul--no-style">
            
            <?php foreach($this->elements->formaciones_areas()->result() as $p): ?>
              <li <?= $p->id==$detail->id?' class="active"':'' ?>>
                <a href="<?= $p->link ?>">
                  <?= $p->titulo ?>
                </a>
              </li>
            <?php endforeach ?>
            
          </ul>
        </div>
      </div>
      <div class="col-lg-9 col-md-8">
        <div class="service-text m-t-50">
          <div class="body__item formacion__inner-body" style="background: #222222;margin: 0;display: inline-block;padding: 0px;margin-bottom: 20px;">
            <div class="box-head box-head--border" style="margin-bottom: 0;">
              <img alt="Icon 1" src="<?= $detail->icono ?>">
            </div>
          </div>
          <?= $detail->texto ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Service Content -->
<!-- Our Process -->
<?php
  $cursos = $this->elements->cursos(array('formaciones_areas_id'=>$detail->id));
  if($cursos->num_rows()>0):
?>
<section class="process-page">  
  <h2 class="title title-3 title-3--center">
    Curs <?= $detail->titulo ?>
  </h2>
  <div class="container">
    
    <div class="row">     
      <?php foreach($cursos->result() as $n=>$p): ?>
        <div class="col-lg-4 col-md-6">
                <div class="service-list__item img-blog">
                  <a href="<?= $p->link ?>">
                    <img alt="Service 1" src="<?= $p->foto ?>"></a>
                    <div class="service-list__text">
                      <h5>
                        <a href="<?= $p->link ?>"><?= $p->titulo ?></a>
                      </h5>                     
                      <p>
                        <?= empty($p->subtitulo)?cortar_palabras(strip_tags($p->descripcion),20):$p->subtitulo; ?> 
                        <a href="<?= $p->link ?>" style="color: #db5c38;"><u><?= l('Veure més') ?></u></a>
                      </p>
                    </div>
                </div>
            </div>
      <?php endforeach ?>
    </div>

  </div>
</section>
<?php endif ?>
<!-- Our Process -->

<?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
<?php $this->load->view($this->theme.'footer',array(),FALSE,'paginas'); ?>
<?php $this->load->view('scripts',array(),FALSE,'paginas'); ?>