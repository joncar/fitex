<li>
  <a href="[base_url]qui-som.html">
    <?= l('qui som') ?>
  </a>
</li>
<li class="li-has-sub">
  <a href="[base_url]projectes.html">
    <?= l('projectes') ?>
  </a>
  <ul class="sub-menu ul--no-style">
    <?php foreach($this->elements->get_proyectos()->result() as $p): ?>
      <li>
        <a href="<?= $p->url ?>">
          <?= $p->nombre ?>
        </a>
      </li>
    <?php endforeach ?>
    
  </ul>
</li>
<li class="li-has-sub">
  <a href="[base_url]serveis.html">
    <?= l('serveis') ?>
  </a>
  <ul class="sub-menu ul--no-style">
    <?php foreach($this->elements->servicios()->result() as $s): ?>
      <li>
        <a href="<?= $s->url ?>">
          <?= $s->nombre ?>
        </a>
      </li>
    <?php endforeach ?>
              
  </ul>
</li>
<li>
  <a href="[base_url]formacio.html">
    <?= l('formacio') ?>
  </a>
</li>
<li>
  <a href="[base_url]blog">
    <?= l('noticies') ?>
  </a>
</li>
<li>
  <a href="[base_url]contacte.html">
    <?= l('contacte') ?>
  </a>
</li>
