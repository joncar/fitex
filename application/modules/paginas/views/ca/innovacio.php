[menu]
<!-- Navigation -->
<section class="navigation">
      <div class="parallax parallax--ser-li" style="background-image: url('[base_url]theme/theme/img/bg-head-innovacio.jpg');">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= l('innovacio') ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]"><?= l('inici') ?></a>
                </li>
                <span>/</span>
                <li>
                  <?= l('qui som') ?>
                </li>
                <span>/</span>
                <li class="active">
                 <?= l('innovacio') ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- Service Content -->
    <section class="service-content p-t-50">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-4">
            <?php $this->load->view($this->theme.'_qui_som',array('active'=>2)); ?>
            <div class="contact2__item">
                <p>
                  We are the reliable partner to help you complete the work
                </p>
                <div>
                  <a href="<?= base_url() ?>contacte.html" class="au-btn au-btn--big au-btn--pill au-btn--yellow au-btn--white">Contact now</a>
                </div>
              </div>
          </div>
          <div class="col-lg-9 col-md-8">
            <div class="service-text m-t-50">
              <h5>
                INNOVACIó
              </h5>
              <p class="m-b-35">
                This is a sed ut perspiciatis unde omnis iste natus error sit volupa accusantium doloremque laudantium, totam rem aperiam,
                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae alt vitae dicta sunt explicabo. Nemo
                enim ipsam voluptatem quiaai voluptas sit aspernatur aut odit aut fugit, sed quia. Sed ut perspiciatis unde
                omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae
                ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
                quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.
              </p>
              <div>
                <img alt="Service Big" src="[base_url]theme/theme/img/service-28.jpg">
              </div>

              <!-- 
<div class="service-img-wrap">
                <div class="service-img">
                  <a href="[base_url]theme/theme/img/service-34.jpg" data-lightbox="service2">
                    <img alt="Service Thumb 1" src="[base_url]theme/theme/img/service-34.jpg">
                  </a>
                </div>
                <div class="service-img">
                  <a href="[base_url]theme/theme/img/service-35.jpg" data-lightbox="service2">
                    <img alt="Service Thumb 2" src="[base_url]theme/theme/img/service-35.jpg">
                  </a>
                </div>
                <div class="service-img">
                  <a href="[base_url]theme/theme/img/service-36.jpg" data-lightbox="service2">
                    <img alt="Service Thumb 3" src="[base_url]theme/theme/img/service-36.jpg">
                  </a>
                </div>
                <div class="service-img">
                  <a href="[base_url]theme/theme/img/service-37.jpg" data-lightbox="service2">
                    <img alt="Service Thumb 4" src="[base_url]theme/theme/img/service-37.jpg">
                  </a>
                </div>
 -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Service Content -->
    
        <!-- Blog Grid 1 -->
    <div class="blog1">
      <div class="container">
      <h2 class="title title-3 title-3--center">
                ULTIMES NOTÍCIES
              </h2>
        <div class="row">
        
          <div class="col-md-12">
            
          </div>
        </div>
        <div id="isotope-grid" class="clearfix">
          <div class="col-md-6 item design">
          
            <div class="blog-item">
              <div class="img-blog">
                <a href="#">
                  <img alt="Blog 1" src="[base_url]theme/theme/img/blog-14.jpg">
                </a>
              </div>
              <div class="blog-content">
                <h4 class="blog-title">
                  <a href=""><em class="author">SUDOTEX</em><br>Xarxa de cooperació transnacional al sector</a>
                </h4>
                <p class="blog-meta">
                  <em class="author">By AThony Lee</em>
                  <em class="cate">Design Knowledge</em>
                  <em class="time">Apr 24,2017</em>
                </p>
                <p>
                  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet ....
                </p>
              </div>
            </div>
          </div>
          <!-- End Item -->
          <div class="col-md-6 item exper">
            <div class="blog-item">
              <div class="img-blog">
                <a href="#">
                  <img alt="Blog 2" src="[base_url]theme/theme/img/blog-15.jpg">
                </a>
              </div>
              <div class="blog-content">
                <h4 class="blog-title">
                  <a href=""><em class="author">RETAIL 2020</em><br>Projecte IRC-Iniciatives de reforç competitiu 2016</a>
                </h4>
                <p class="blog-meta">
                  <em class="author">By Mike Song</em>
                  <em class="cate">Interior Experiences</em>
                  <em class="time">Dec 14,2017</em>
                </p>
                <p>
                  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet ....
                </p>
              </div>
            </div>
          </div>
          <!-- End Item -->
          <div class="col-md-6 item trick">
            <div class="blog-item">
              <div class="img-blog">
                <a href="#">
                  <img alt="Blog 3" src="[base_url]theme/theme/img/blog-16.jpg">
                </a>
              </div>
              <div class="blog-content">
                <h4 class="blog-title">
                  <a href=""><em class="author">RETAIL 2020</em><br>5 things will make your room more beautiful</a>
                </h4>
                <p class="blog-meta">
                  <em class="author">By Jawn Ha</em>
                  <em class="cate">Tricks and Tips</em>
                  <em class="time">Nov 24,2017</em>
                </p>
                <p>
                  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet ....
                </p>
              </div>
            </div>
          </div>
          <!-- End Item -->
          <div class="col-md-6 item design">
            <div class="blog-item">
              <div class="img-blog">
                <a href="#">
                  <img alt="Blog 4" src="[base_url]theme/theme/img/blog-17.jpg">
                </a>
              </div>
              <div class="blog-content">
                <h4 class="blog-title">
                  <a href=""><em class="author">RETAIL 2020</em><br>less is more - the rude of interior design</a>
                </h4>
                <p class="blog-meta">
                  <em class="author">By Lake Louthor</em>
                  <em class="cate">Design Knowledge</em>
                  <em class="time">Apr 24,2017</em>
                </p>
                <p>
                  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet ....
                </p>
              </div>
            </div>
          </div>
          <!-- End Item -->
          <div class="col-md-6 item qna">
            <div class="blog-item">
              <div class="img-blog">
                <a href="#">
                  <img alt="Blog 5" src="[base_url]theme/theme/img/blog-18.jpg">
                </a>
              </div>
              <div class="blog-content">
                <h4 class="blog-title">
                  <a href=""><em class="author">RETAIL 2020</em><br>why black and white always are the best color?</a>
                </h4>
                <p class="blog-meta">
                  <em class="author">By Lucy Green</em>
                  <em class="cate">Q&A</em>
                  <em class="time">Apr 14,2017</em>
                </p>
                <p>
                  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet ....
                </p>
              </div>
            </div>
          </div>
          <!-- End Item -->
          <div class="col-md-6 item exper">
            <div class="blog-item">
              <div class="img-blog">
                <a href="#">
                  <img alt="Blog 6" src="[base_url]theme/theme/img/blog-19.jpg">
                </a>
              </div>
              <div class="blog-content">
                <h4 class="blog-title">
                  <a href=""><em class="author">RETAIL 2020</em><br>ALL items you need to have in your room 2017</a>
                </h4>
                <p class="blog-meta">
                  <em class="author">By Lebrond</em>
                  <em class="cate">Interior Experiences</em>
                  <em class="time">jan 09,2017</em>
                </p>
                <p>
                  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet ....
                </p>
              </div>
            </div>
          </div>
          <!-- End Item -->
          <div class="col-md-6 item design">
            <div class="blog-item">
              <div class="img-blog">
                <a href="#">
                  <img alt="Blog 7" src="[base_url]theme/theme/img/blog-20.jpg">
                </a>
              </div>
              <div class="blog-content">
                <h4 class="blog-title">
                  <a href=""><em class="author">RETAIL 2020</em><br>best office interior in us-uk</a>
                </h4>
                <p class="blog-meta">
                  <em class="author">By Mike song</em>
                  <em class="cate">Interior Experiences</em>
                  <em class="time">dec 14,2017</em>
                </p>
                <p>
                  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet ....
                </p>
              </div>
            </div>
          </div>
          <!-- End Item -->
          <div class="col-md-6 item trick">
            <div class="blog-item">
              <div class="img-blog">
                <a href="#">
                  <img alt="Blog 8" src="[base_url]theme/theme/img/blog-21.jpg">
                </a>
              </div>
              <div class="blog-content">
                <h4 class="blog-title">
                  <a href=""><em class="author">RETAIL 2020</em><br>time to change your place</a>
                </h4>
                <p class="blog-meta">
                  <em class="author">By Jawn Ha</em>
                  <em class="cate">Tricks and Tips</em>
                  <em class="time">nov 24,2017</em>
                </p>
                <p>
                  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
                  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet ....
                </p>
              </div>
            </div>
          </div>
          <!-- End Item -->
        </div>
      </div>
    </div>
    <!-- End Blog Grid 1 -->
<?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
    [footer]
    [scripts]