[menu]
 <!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--ser-li">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                Internacionalització
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]">Inici</a>
                </li>
                <span>/</span>
                <li>
                  Qui Som
                </li>
                <span>/</span>
                <li class="active">
                 Internacionalització
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
<!-- Service Content -->
<section class="service-content p-t-50">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-4">
				<?php $this->load->view($this->theme.'_qui_som',array('active'=>1)); ?>
			</div>
			<div class="col-lg-9 col-md-8">
				<div class="service-text m-t-50">
					<h5>
					INTERNACIONALITZACIÓ
					</h5>
					<p class="m-b-35">
						This is a sed ut perspiciatis unde omnis iste natus error sit volupa accusantium doloremque laudantium, totam rem aperiam,
						eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae alt vitae dicta sunt explicabo. Nemo
						enim ipsam voluptatem quiaai voluptas sit aspernatur aut odit aut fugit, sed quia. Sed ut perspiciatis unde
						omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae
						ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
						quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.
					</p>
					<p>
						This is a sed ut perspiciatis unde omnis iste natus error sit volupa accusantium doloremque laudantium, totam rem aperiam,
						eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae alt vitae dicta sunt explicabo. Nemo
						enim ipsam voluptatem quiaai voluptas sit aspernatur aut odit aut fugit, sed quia. Sed ut perspiciatis unde
						omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae
						ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
						quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.
					</p>
					<!--
					<div>
						<img alt="Service Big" src="[base_url]theme/theme/img/service-28.jpg">
					</div>
					-->
					<!--
					<div class="service-img-wrap">
						<div class="service-img">
							<a href="[base_url]theme/theme/img/service-34.jpg" data-lightbox="service2">
								<img alt="Service Thumb 1" src="[base_url]theme/theme/img/service-34.jpg">
							</a>
						</div>
						<div class="service-img">
							<a href="[base_url]theme/theme/img/service-35.jpg" data-lightbox="service2">
								<img alt="Service Thumb 2" src="[base_url]theme/theme/img/service-35.jpg">
							</a>
						</div>
						<div class="service-img">
							<a href="[base_url]theme/theme/img/service-36.jpg" data-lightbox="service2">
								<img alt="Service Thumb 3" src="[base_url]theme/theme/img/service-36.jpg">
							</a>
						</div>
						<div class="service-img">
							<a href="[base_url]theme/theme/img/service-37.jpg" data-lightbox="service2">
								<img alt="Service Thumb 4" src="[base_url]theme/theme/img/service-37.jpg">
							</a>
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Service Content -->
<!-- Our Process -->
<section class="process-page">  <h2 class="title title-3 title-3--center">
	PROJECTES D'INTERNACIONALITZACIÓ
	</h2>
	<div class="container">
		<div class="process-item">
			<div class="process__left wow fadeInLeft" data-wow-delay="1s">
				<div class="pro__img">
					<img alt="Process 1" src="[base_url]theme/theme/img/process-05.jpg">
				</div>
			</div>
			<div class="process__right pro__text-wrap bg-f8 wow fadeInRight" data-wow-delay="1s">
				<div class="pro__text">
					<h2>
					<span>01</span>
					REINTEX
					</h2>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore et dolore magna aliqua.
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</p>
					
					<div class="row no-gutters">
						<div class="col-md-6">
							<div class="li-item">
								<i class="fa fa-check-circle" aria-hidden="true"></i>
								Duis aute irure dolor in
							</div>
							<div class="li-item">
								<i class="fa fa-check-circle" aria-hidden="true"></i>
								Excepteur sint occaecat
							</div>
						</div>
						<div class="col-md-6">
							<div class="li-item">
								<i class="fa fa-check-circle" aria-hidden="true"></i>
								Sunt in culpa qui
							</div>
							<div class="li-item">
								<i class="fa fa-check-circle" aria-hidden="true"></i>
								Ut enim ad minima veniam
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="process-item">
			<div class="process__left pro__text-wrap bg-f8 wow fadeInLeft" data-wow-delay="1s">
				<div class="pro__text">
					<h2>
					<span>02</span>
					CUPONS A LA INTERNACIONALITZACIÓ
					</h2>
					<p class="mb-0">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore et dolore magna aliqua.
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</p>
					<p>
						Sed ut perspiciatis unde omnis iste natus error sit voluptatem huska ax accuntium doloremque laudantium, totam rem aperiam,
						eaque ipsam .quae ab illo inven
					</p>
				</div>
			</div>
			<div class="process__right wow fadeInRight" data-wow-delay="1s">
				<div class="pro__img">
					<img alt="Process 2" src="[base_url]theme/theme/img/process-06.jpg">
				</div>
			</div>
		</div>
		<div class="process-item">
			<div class="process__left wow fadeInLeft">
				<div class="pro__img">
					<img alt="Process 3" src="[base_url]theme/theme/img/process-07.jpg">
				</div>
			</div>
			<div class="process__right pro__text-wrap bg-f8 wow fadeInRight">
				<div class="pro__text">
					<h2>
					<span>03</span>
					ideal
					</h2>
					<p class="m-b-30">
						Excepteur sint occaecat cupidatat non proident, sunt in culpa quison al officia deserunt mollit anim id est laborum.
					</p>
					<div class="row no-gutters pro-bar-wrap">
						<div class="col-md-3">
							<p class="bar__title">Creative</p>
							<p class="bar__title">Effective</p>
							<p class="bar__title">Suport</p>
						</div>
						<div class="col-md-9">
							<div class="pro-bar-container color-333">
								<div class="pro-bar color-e1" data-pro-bar-percent="80" data-pro-bar-delay="500">
								</div>
							</div>
							<div class="pro-bar-container color-333 m-y-15">
								<div class="pro-bar color-e1" data-pro-bar-percent="90" data-pro-bar-delay="500">
								</div>
							</div>
							<div class="pro-bar-container color-333">
								<div class="pro-bar color-e1" data-pro-bar-percent="75" data-pro-bar-delay="500">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="process-item">
			<div class="process__left pro__text-wrap bg-f8 wow fadeInLeft">
				<div class="pro__text">
					<h2 class="m-b-20">
					<span>04</span>
					contruct
					</h2>
					<p class="m-b-0">
						<span>saving money: </span>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore et dolore magna aliqua.
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</p>
					<p>
						<span>fast: </span>
						Sed ut perspiciatis unde omnis iste natus error sit voluptatem huska ax accuntium doloremque laudantium, totam rem aperiam,
						eaque ipsam .quae ab illo inven
					</p>
				</div>
			</div>
			<div class="process__right wow fadeInRight">
				<div class="pro__img">
					<img alt="Process 4" src="[base_url]theme/theme/img/process-08.jpg">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Our Process -->
<!-- End Blog Grid 1 -->
<?php $this->load->view($this->theme.'_contacte_bar_1',array(),FALSE,'paginas'); ?>
[footer]
[scripts]