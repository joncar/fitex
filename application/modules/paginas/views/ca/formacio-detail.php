[menu]
<!-- Navigation -->
    <section class="navigation">
      <div class="parallax parallax--ser-li"  style="background-image: url('[base_url]theme/theme/img/bg-head-formacio.jpg');">
        <div class="container clearfix">
          <div class="row">
            <div class="col-md-12">
              <h2>
                <?= l('Àrea Tècnica i Tecnològica') ?>
              </h2>
              <ul class="breadcrumbs ul--inline ul--no-style">
                <li>
                  <a href="[base_url]"><?= l('inici') ?></a>
                </li>
                <span>/</span>
                <li class="active">
                  <?= l('formacio') ?>
                </li>
                <span>/</span>
                <li class="active">
                  <?= l('Àrea Tècnica i Tecnològica') ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Navigation -->
    <!-- Port 2 -->
    <section class="port2">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-12">
            <div class="port__text">
              <h3><?= l('Informació') ?></h3>
              <p>
                <?= l('informacio2') ?>
              </p>
            </div>
            
              <!-- 
<ul class="port__info-list clearfix ul~~no-style">
                <li>
                  <span class="port__info-title">Client</span>
                  <span class="port__info-value">Loki Resort</span>
                </li>
                <li>
                  <span class="port__info-title">Acreage</span>
                  <span class="port__info-value">400.00 m
                    <sup>2</sup>
                  </span>
                </li>
                <li>
                  <span class="port__info-title">Date</span>
                  <span class="port__info-value">April 24 2017</span>
                </li>
              </ul>
 -->
              <!-- 
<div class="social~~port">
                <a href="">
                  <i class="zmdi zmdi-facebook"></i>
                </a>
                <a href="">
                  <i class="zmdi zmdi-dribbble"></i>
                </a>
                <a href="">
                  <i class="zmdi zmdi-google"></i>
                </a>
                <a href="">
                  <i class="zmdi zmdi-twitter"></i>
                </a>
                <a href="">
                  <i class="zmdi zmdi-instagram"></i>
                </a>
              </div>
 -->
            
            <!-- End Port Info -->
          </div>
          <div class="col-lg-8 col-md-12">
            <div class="port__img">
              <img alt="Portfolio 1" src="[base_url]theme/theme/img/port-big-02.jpg">
            </div>
            <div class="port__img m-b-0">
              <img alt="Portfolio 2" src="[base_url]theme/theme/img/port-big-03.jpg">
            </div>
          </div>
        </div>
        <!-- Nav -->
        <div class="port-nav-wrap">
          <div class="container">
            <div class="col-md-12 clearfix">
              <i class="zmdi zmdi-view-module"></i>
              <a href="#" class="nav-port pull-left">
                <i class="zmdi zmdi-arrow-left"></i>
              </a>
              <a href="#" class="nav-port pull-right">
                <i class="zmdi zmdi-arrow-right"></i>
              </a>
            </div>
          </div>
        </div>
        <!-- End Nav -->
      </div>
      <div class="service-list" style="margin-bottom: 0; padding-bottom: 30px">
      <?php $this->load->view($this->theme.'_contacte_bar_2',array(),FALSE,'paginas'); ?>
    </div>
    </section>
    <!-- End Port 2 -->
    
    [footer]
    [scripts]