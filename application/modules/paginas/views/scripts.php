 <!-- =================== PLUGIN JS ==================== -->
  <script src="<?= base_url() ?>theme/theme/vendor/jquery-3.2.1.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>theme/theme/vendor/slick/slick.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>theme/theme/vendor/wow/wow.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>theme/theme/vendor/lightbox2/src/js/lightbox.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>theme/theme/vendor/bootstrap4/popper.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>theme/theme/vendor/bootstrap4/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>theme/theme/vendor/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>theme/theme/vendor/revolution/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>theme/theme/vendor/revolution/jquery.themepunch.tools.min.js" type="text/javascript"></script>


  <script src="<?= base_url() ?>theme/theme/vendor/counter-up/jquery.waypoints.min.js" type="text/javascript"></script>
  <script src="<?= base_url() ?>theme/theme/vendor/counter-up/jquery.counterup.min.js" type="text/javascript"></script>
  <!-- Local Revolution -->
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/vendor/revolution/local/revolution.extension.migration.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/vendor/revolution/local/revolution.extension.actions.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/vendor/revolution/local/revolution.extension.carousel.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/vendor/revolution/local/revolution.extension.kenburn.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/vendor/revolution/local/revolution.extension.layeranimation.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/vendor/revolution/local/revolution.extension.navigation.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/vendor/revolution/local/revolution.extension.parallax.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/vendor/revolution/local/revolution.extension.slideanims.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/vendor/revolution/local/revolution.extension.video.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      lightbox.option({
        'resizeDuration': 200,
        'wrapAround': false,
        'alwaysShowNavOnTouchDevices': true,
      });
    });
  </script>

  <!-- =================== CUSTOM JS ==================== -->
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/js/main.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/js/revo-custom.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/js/wow-custom.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/js/count.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/js/slick-custom.js"></script>
  <script src="<?= base_url() ?>theme/theme/vendor/isotope/isotope.pkgd.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?= base_url() ?>theme/theme/js/isotope-custom.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>js/frame.js?v=1"></script>

