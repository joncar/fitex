<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }       

        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){            
            $theme = $this->theme;
            $params = $this->uri->segments;
            //Existe?            
            if(file_exists(APPPATH.'modules/paginas/views/'.$this->theme.$url.'.php')){
                $page = $this->load->view($theme.$url,array(),TRUE);
            }else{
                $idiomas = $this->db->get('ajustes')->row()->idiomas;
                $idiomas = explode(',',$idiomas);
                foreach($idiomas as $i){
                    $i = trim($i);
                    if(file_exists(APPPATH.'modules/paginas/views/'.$i.'/'.$url.'.php')){
                        $page = $this->load->view($i.'/'.$url,array(),TRUE);
                        $_SESSION['lang'] = $i;
                        $this->theme = $i.'/';
                    }
                }
            }
            if(empty($page)){
                throw new exception('Pagina web no encontrada',404);
            }
            $title = $this->db->get_where('idiomas',array('identificador'=>$url,'idioma'=>$_SESSION['lang']));
            $title = $title->num_rows()>0?$title->row()->texto:$url;
            $this->load->model('querys');
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$page,
                    'link'=>$url,
                    'url'=>$url,
                    'title'=>ucfirst(str_replace('-',' ',$title))
                )
            );
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                                            
                $page = $this->load->view($url,array(),TRUE);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){             
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');            
            $this->form_validation->set_rules('politicas','Politicas','required');                        
            $this->form_validation->set_rules('token','Captcha','required');                        
            if($this->form_validation->run()){ 
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['token'])){
                    $_SESSION['msj'] = $this->error('ERROR! Captcha no accepted');
                }else{                
                    $datos = $_POST;
                    if(!empty($_POST['extras'])){
                        unset($datos['extras']);
                        $datos['extras'] = '';
                        foreach($_POST['extras'] as $n=>$v){
                            $datos['extras'].= '<br/><b>'.l($n).': </b> '.$v.'';
                        }
                    }else{
                        $datos['extras'] = '';
                    }
                    if(empty($_POST['titulo'])){
                        $datos['titulo'] = 'Correo sin titulo enviado desde la web';
                    }else{
                        $datos['titulo'] = l($_POST['titulo']);
                    }
                    if(empty($_POST['asunto'])){
                        $datos['asunto'] = '';
                    }
                    $email = 'general@fitex.es';
                    if(!empty($_POST['to'])){                        
                        $email = $_POST['to'];
                    }else{
                        $datos['to'] = $email;
                    }
                    $this->enviarcorreo((object)$datos,6,$email);
                    $this->enviarcorreo((object)$datos,6,$datos['email']);
                    $_SESSION['msj'] = $this->success(l('contact_success'));                    
                }
            }else{                
               $_SESSION['msj'] = $this->error(l('contact_error'));
            }

            echo $_SESSION['msj'];
            unset($_SESSION['msj']);
        }
        
        function subscribir(){
            $err = 'error';
            $this->form_validation->set_rules('email','Email','required|valid_email');
            //$this->form_validation->set_rules('politicas','Políticas','required');
            if($this->form_validation->run()){
                
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                    $_SESSION['msj2'] = $this->success(l('subscribe_success'));                    
                    //$this->enviarcorreo((object)$_POST,10);
                }else{
                    $_SESSION['msj2'] = $this->error(l('subscribe_error'));
                }
            }else{
                $_SESSION['msj2'] = $this->error($this->form_validation->error_string());
            }
            echo $_SESSION['msj2'];
            unset($_SESSION['msj2']);            
        }
        
        function unsubscribe(){
            if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('email'=>$_POST['email']));
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }            
                $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }
        }

        function search(){
            $domain = base_url();
            $domain = explode('/',$domain);
            $domain = str_replace('www.','',$domain[2]);
            if(!empty($_GET['q'])){
                if(empty($_SESSION[$_GET['q']])){
                    $_SESSION[$_GET['q']] = file_get_contents('http://www.google.es/search?ei=meclXLnwCNma1fAPgbCYCA&q=site%3A'.$domain.'+'.urlencode($_GET['q']).'&oq=site%3A'.$domain.'+'.urlencode($_GET['q']).'&gs_l=psy-ab.3...10613.16478..16743...0.0..0.108.1753.20j3......0....1..gws-wiz.0XRgmCZL0TA');                
                }

                $result = $_SESSION[$_GET['q']];                
                preg_match_all('@<div class=\"ZINbbc xpd O9g5cc uUPGi\">(.*)</div>@si',$result,$result);
                $resultado = $result[0][0];
                $resultado = explode('<footer>',$resultado);
                $resultado = $resultado[0];
                $resultado = str_replace('/url?q=','',$resultado);
                $resultado = explode('<div class="ZINbbc xpd O9g5cc uUPGi">',$resultado);                
                foreach($resultado as $n=>$r){
                    if(strpos($r,'/search?q=site:')){
                        unset($resultado[$n]);
                        continue;
                    }
                    $resultado[$n] = $r;                    
                    $resultado[$n] = substr($r,0,strpos($r,'&'));
                    $pos = strpos($r,'">')+2;
                    $rr = substr($r,$pos);
                    $pos = strpos($rr,'">');
                    $rr = substr($rr,$pos);                    
                    $resultado[$n].= $rr;
                    $resultado[$n] = '<div class="ZINbbc xpd O9g5cc uUPGi">'.$resultado[$n];
                    $resultado[$n] = utf8_encode($resultado[$n]);
                }
                $resultado = implode('',$resultado);
                $this->loadView(array(
                    'view'=>'read',
                    'page'=>$this->load->view($this->theme.'search',array('resultado'=>$resultado),TRUE,'paginas'),
                    'result'=>$resultado,
                    'title'=>'Resultado de busqueda'
                ));

            }
        }

        function sitemap(){
            $pages = array(
                base_url(),
                base_url().'qui-som.html',
                base_url().'projectes.html',
                base_url().'serveis.html',
                base_url().'formacio.html',
                base_url().'blog',
                base_url().'avis-legal.html',
            );

            
            foreach($this->elements->servicios()->result() as $s){
                $pages[] = $s->url;            
            }

            foreach($this->elements->get_proyectos()->result() as $p){
                $pages[] = $p->url;
                foreach($p->proyectos->result() as $pp){
                    $pages[] = $pp->url;
                }
            }

            foreach($this->elements->formaciones_areas()->result() as $f){
                $pages[] = $f->link;            
            }

            foreach($this->elements->cursos()->result() as $f){
                $pages[] = $f->link;            
            }

            $site = '<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
            foreach($pages as $p){
                $site.= '
                <url>
                      <loc>'.trim($p).'</loc>
                      <lastmod>'.date("Y-m-d").'T11:43:00+00:00</lastmod>
                      <priority>1.00</priority>
                </url>';
            }
            $site.= '</urlset>';
            ob_end_clean();
            ob_end_flush();
            header('Content-Type: application/xml');
            echo $site;
        }
    }
