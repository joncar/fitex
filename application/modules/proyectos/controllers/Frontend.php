<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }

        function ver($id){  
            $this->db->where('url',$id);                        
            $detail = $this->elements->get_proyectos();
        	if($detail->num_rows()>0){
        		$detail = $detail->row();        		
        		$this->loadView(array(
        			'view'=>'list',
        			'detail'=>$detail,
        			'url'=>'proyectos',
        			'title'=>$detail->nombre
        		));
        	}else{
        		redirect('main');
        	}
        }
        function detalle($id){          
            $detail = $this->elements->get_proyecto(array('url'=>$id));
            if($detail->num_rows()>0){
                $detail = $detail->row();               
                $this->loadView(array(
                    'view'=>'ver',
                    'detail'=>$detail,
                    'url'=>'proyectos',
                    'title'=>$detail->titulo
                ));
            }
        }
    }
?>
