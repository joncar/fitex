<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function categoria_proyectos(){
            
            $crud = $this->crud_function('','');                
            $crud->field_type('foto','image',array('path'=>'theme/theme/img','width'=>'370px','height'=>'208px'));
            $crud->field_type('miniatura','image',array('path'=>'theme/theme/img','width'=>'1600px','height'=>'1168px'));
            $crud->set_order('orden');
            $crud->unset_columns('idiomas');
            $crud->add_action('Proyectos','',base_url('proyectos/admin/proyectos').'/');
            $crud->set_traducir();
            $crud->add_action('Traducir','',base_url('proyectos/admin/categoria_proyectos/traducir').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        function proyectos($x = ''){
            
            $crud = $this->crud_function('','');  
            $crud->field_type('idioma','dropdown',array('es'=>'Castellano','ca'=>'Catalá'));           
            $crud->field_type('foto','image',array('path'=>'img/proyectos','width'=>'1600px','height'=>'985px'));
            $crud->columns('foto','categoria_proyectos_id','titulo','url');
            $crud->set_traducir();
            $crud->add_action('Traducir','',base_url('proyectos/admin/proyectos/'.$x.'/traducir').'/');
            $crud->set_order('orden');
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
