<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function blog(){
            $crud = $this->crud_function('','');
            $crud->field_type('foto','image',array('path'=>'theme/theme/img','width'=>'1600px','height'=>'903px'));
            $crud->field_type('foto_main','image',array('path'=>'theme/theme/img','width'=>'1600px','height'=>'2121px'));
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud->field_type('blog_subcategorias_id','hidden',1);
            $crud->columns("blog_categorias_id","foto","titulo","tags","fecha","idioma");            
            $crud->field_type('user','string',$this->user->nombre);
            $crud->field_type('idiomas','hidden');
            $crud->add_action('Traducir','',base_url('blog/admin/blog/traducir').'/');
            $crud->unset_columns('idiomas');
            $crud->set_clone();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function blog_categorias(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Categoria');
            $crud->display_as('blog_categorias_nombre','Nombre');
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalan','es'=>'Español'));
            $crud = $crud->render();
            $crud->title = 'Categorias';
            $this->loadView($crud);
        }
        
        public function clonarEntrada($id){
            if(is_numeric($id)){
                $entry = new Bdsource();
                $entry->where('id',$id);
                $entry->init('blog',TRUE,'entrada');
                $data = $this->entrada;
                $entry->save($data,null,TRUE);
                header("Location:".base_url('blog/admin/blog/edit/'.$entry->getid()));
            }
        }
    }
?>
