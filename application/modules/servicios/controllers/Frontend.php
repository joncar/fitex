<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }

        function ver($id){        	
            if(!empty($id)){
            	$servicio = $this->db->get_where('servicios',array('url'=>$id));
            	if($servicio->num_rows()>0){
                    $servicio = $this->elements->servicios(array('id'=>$servicio->row()->id));
                    $servicio = $this->traduccion->transform($servicio);
                    $servicio = $servicio->row();
            		$this->loadView(array('view'=>'ver','servicio'=>$servicio,'url'=>'serveis','title'=>$servicio->nombre));
            	}else{
            		redirect('main');
            	}
            }
        }

        function formaciones($id){
            $url = $id;
            $id = explode('-',$id);
            $id = $id[0];

            if(is_string($id)){
                $this->db->where('url',$url);
                //$this->db->or_where('getUrlEs(blog.id)',$url);
                $blog = $this->db->get_where('formaciones_areas');
                if($blog->num_rows()>0){
                    $id = $blog->row()->id;
                }
            }

            if(is_numeric($id)){
                $servicio = $this->elements->formaciones_areas(array('formaciones_areas.id'=>$id));
                if($servicio->num_rows()>0){
                    $servicio = $servicio->row();                    
                    $this->loadView(array('view'=>'formaciones_areas','detail'=>$servicio,'title'=>strip_tags($servicio->titulo),'description'=>cortar_palabras(strip_tags($servicio->descripcion),10)));
                }else{
                    throw new Exception('No se encuentra la entrada solicitada',404);    
                }
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        } //End view

         function curso($id){
            $url = $id;
            $id = explode('-',$id);
            $id = $id[0];

            if(is_string($id)){
                $this->db->where('url',$url);
                //$this->db->or_where('getUrlEs(blog.id)',$url);
                $blog = $this->db->get_where('cursos');
                if($blog->num_rows()>0){
                    $id = $blog->row()->id;
                }
            }

            if(is_numeric($id)){
                $servicio = $this->elements->cursos(array('cursos.id'=>$id));
                if($servicio->num_rows()>0){
                    $servicio = $servicio->row();                    
                    $this->loadView(array('view'=>'cursos','detail'=>$servicio,'title'=>strip_tags($servicio->titulo),'description'=>cortar_palabras(strip_tags($servicio->descripcion),10)));
                }else{
                    throw new Exception('No se encuentra la entrada solicitada',404);    
                }
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        } //End view
}
?>
