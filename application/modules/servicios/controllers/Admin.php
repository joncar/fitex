<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function servicios(){
            
            $crud = $this->crud_function('','');  
            $crud->field_type('miniatura','image',array('path'=>'img/servicios','width'=>'370px','height'=>'208px'));
            $crud->field_type('fotos','gallery',array('path'=>'img/servicios','width'=>'840px','height'=>'450px'));

            $crud->field_type('icono_main','image',array('path'=>'theme/theme/img/icon','width'=>'63px','height'=>'54px'));
            $crud->field_type('foto_main','image',array('path'=>'theme/theme/img','width'=>'920px','height'=>'926px'));
            $crud->field_type('idiomas','hidden');
            $crud->add_action('Traducir','',base_url('servicios/admin/servicios/traducir').'/');
            $crud->unset_columns('idiomas');
            $crud->set_order('orden');
            $crud->columns('miniatura','nombre');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        function ultimos_trabajos(){
            
            $crud = $this->crud_function('','');              
			$crud->field_type('foto','image',array('path'=>'img/servicios','width'=>'1920px','height'=>'1080px'));
            $crud->field_type('miniatura','image',array('path'=>'img/servicios','width'=>'500px','height'=>'625px'));
			$crud->field_type('idioma','dropdown',array('es'=>'Castellano','ca'=>'Catalán'));            
			$crud->field_type('tags','tags');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        function servicios_detalles($x = ''){
            $crud = $this->crud_function('','');   
            $crud->field_type('servicios_id','hidden',$x)
                 ->where('servicios_id',$x);           
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function comte_anuals($x = ''){
            $crud = $this->crud_function('','');   
            $crud->field_type('foto','image',array('path'=>'img/comte_anuals','width'=>'370px','height'=>'208px')); 
            $crud->set_field_upload('fichero','files');         
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function formaciones_areas($x = ''){
            $crud = $this->crud_function('','');
            $crud->field_type('icono','image',array('path'=>'img/formaciones','width'=>'44px','height'=>'33px'));
            $crud->add_action('cursos','',base_url('servicios/admin/cursos/').'/');
            $crud->set_order('orden');
            $crud->field_type('idiomas','hidden');
            $crud->add_action('Traducir','',base_url('servicios/admin/formaciones_areas/traducir').'/');
            $crud->unset_columns('idiomas');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function cursos($x = ''){
            $crud = $this->crud_function('','');
            $crud->where('formaciones_areas_id',$x)
                 ->field_type('formaciones_areas_id','hidden',$x)
                 ->unset_columns('formaciones_areas_id');
            $crud->columns('foto','titulo');
            $crud->field_type('foto','image',array('path'=>'img/proyectos','width'=>'1600px','height'=>'985px'));
            $crud->field_type('fotos','gallery',array('path'=>'img/servicios','width'=>'840px','height'=>'450px'));
            $crud->field_type('icono','image',array('path'=>'img/formaciones','width'=>'44px','height'=>'33px'));
            $crud->field_type('foto_main','image',array('path'=>'img/formaciones','width'=>'960px','height'=>'982px'));
            $crud->field_type('idiomas','hidden');
            $crud->add_action('Traducir','',base_url('servicios/admin/cursos/'.$x.'/traducir').'/');
            $crud->unset_columns('idiomas');
            $crud->set_order('orden');                        
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
