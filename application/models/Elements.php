<?php 
class Elements extends CI_Model{
	public $proyectos = array();
	function __construct(){
		parent::__construct();
	}

	function categoria_proyectos($where = array()){		
		$this->db->order_by('categoria_proyectos.orden');
		$prp = $this->db->get_where('categoria_proyectos',$where);
		foreach($prp->result() as $nn=>$pp){
			$prp->row($nn)->miniatura = base_url('theme/theme/img/'.$pp->miniatura);			
			$prp->row($nn)->url = base_url('projectes/'.$pp->url);			
		}
		return $this->traduccion->transform($prp);
	}

	function get_proyectos($where = array()){
		//$where['idioma']=$_SESSION['lang'];
		//if(empty($this->proyectos) || $this->proyectos->where != $where){
			$this->db->order_by('categoria_proyectos.orden','ASC');
			$proyectos = $this->db->get_where('categoria_proyectos',$where);			
			foreach($proyectos->result() as $n=>$p){
				$p = $this->traduccion->traducirObj($p);

				$proyectos->row($n)->proyectos = $this->get_proyecto(array('categoria_proyectos_id'=>$p->id));
				$proyectos->row($n)->url = base_url('projectes/'.$p->url);
				$proyectos->row($n)->descripcion = str_replace('//hipo.tv/fitex/',base_url(),$p->descripcion);
			}			
			$this->proyectos = $proyectos;
			$this->proyectos->where = $where;
		//}
		return $this->proyectos;
	}

	function get_proyecto($where = array()){			
		$this->db->order_by('proyectos.orden','ASC');		
		$prp = $this->db->get_where('proyectos',$where);
		if($prp->num_rows()==0 && !empty($where['url'])){
			$prp = $this->db->get('proyectos');
			foreach($prp->result() as $p){
				$idiomas = json_decode($p->idiomas);				
				if(!empty($idiomas->{$_SESSION['lang']}) && $idiomas->{$_SESSION['lang']}->url==$where['url']){
					$prp = $this->db->get_where('proyectos',array('id'=>$p->id));
				}
			}
		}
		foreach($prp->result() as $nn=>$pp){	
			$pp = $this->traduccion->traducirObj($pp);		
			$prp->row($nn)->foto = base_url('img/proyectos/'.$pp->foto);
			$prp->row($nn)->categoria = $this->db->get_where('categoria_proyectos',array('id'=>$pp->categoria_proyectos_id))->row();
			$prp->row($nn)->url = base_url('projecte/'.$pp->url);
			$prp->row($nn)->descripcion = str_replace('//hipo.tv/fitex/',base_url(),$pp->descripcion);		
			
		}
		return $prp;
	}

	function servicios($where = array()){	
		$this->db->order_by('servicios.orden','ASC');	
		$prp = $this->db->get_where('servicios',$where);
		if($prp->num_rows()==0 && !empty($where['url'])){
			$prp = $this->db->get('servicios');
			foreach($prp->result() as $p){
				$idiomas = json_decode($p->idiomas);				
				if(!empty($idiomas->{$_SESSION['lang']}) && $idiomas->{$_SESSION['lang']}->url==$where['url']){
					$prp = $this->db->get_where('servicios',array('id'=>$p->id));
				}
			}
		}
		foreach($prp->result() as $nn=>$pp){	
			$pp = $this->traduccion->traducirObj($pp);		
			$prp->row($nn)->miniatura = base_url('img/servicios/'.$pp->miniatura);			
			$prp->row($nn)->icono_main = base_url('theme/theme/img/icon/'.$pp->icono_main);			
			$prp->row($nn)->foto_main = base_url('theme/theme/img/'.$pp->foto_main);			
			$prp->row($nn)->url = base_url('servei/'.$pp->url);
			$prp->row($nn)->fotos = explode(',',$pp->fotos);
			$prp->row($nn)->descripcion = str_replace('//hipo.tv/fitex/',base_url(),$pp->descripcion);
			foreach($prp->row($nn)->fotos as $nnn=>$ppp){
				if(!empty($ppp)){
					$prp->row($nn)->fotos[$nnn] = base_url('img/servicios/'.$ppp);
				}			
			}			
			
		}
		return $prp;
	}

	function formaciones_areas($where = array()){	
		$this->db->order_by('formaciones_areas.orden','ASC');	
		$prp = $this->db->get_where('formaciones_areas',$where);
		foreach($prp->result() as $nn=>$pp){
			$prp->row($nn)->icono = base_url('img/formaciones/'.$pp->icono);			
			$prp->row($nn)->link = base_url('formacio/'.$pp->url);
			$prp->row($nn)->cursos = $this->cursos(array('formaciones_areas_id'=>$pp->id),FALSE);
		}
		return $this->traduccion->transform($prp);
	}

	function cursos($where = array(),$ancFormacion = TRUE){	
		$this->db->order_by('cursos.orden','ASC');	
		$prp = $this->db->get_where('cursos',$where);
		foreach($prp->result() as $nn=>$pp){
			$prp->row($nn)->foto = base_url('img/proyectos/'.$pp->foto);			
			$prp->row($nn)->foto_main = base_url('img/formaciones/'.$pp->foto_main);			
			$prp->row($nn)->link = base_url('curs/'.$pp->url);
			if($ancFormacion){
				$prp->row($nn)->formacion = $this->formaciones_areas(array('formaciones_areas.id'=>$pp->formaciones_areas_id))->row();
			}
			$prp->row($nn)->fotos = explode(',',$pp->fotos);
			$prp->row($nn)->icono = base_url('img/formaciones/'.$pp->icono);			
			foreach($prp->row($nn)->fotos as $nnn=>$ppp){
				if(!empty($ppp)){
					$prp->row($nn)->fotos[$nnn] = base_url('img/servicios/'.$ppp);
				}
			}			
		}
		return $this->traduccion->transform($prp);
	}

	function blog($where = array(),$ancFormacion = TRUE){	
		$this->db->order_by('blog.fecha','DESC');	
		$prp = $this->db->get_where('blog',$where);
		foreach($prp->result() as $nn=>$pp){
			$prp->row($nn)->foto = base_url('theme/theme/img/'.$pp->foto);
			$prp->row($nn)->foto_main = base_url('theme/theme/img/'.$pp->foto_main);
			$prp->row($nn)->link = site_url('blog/'.toURL($pp->id.'-'.$pp->titulo));
			$prp->row($nn)->dia = date("d",strtotime($pp->fecha));
			$prp->row($nn)->mes = strftime("%b",strtotime($pp->fecha));
		}
		return $this->traduccion->transform($prp);
	}

	function comte_anuals($where = array(),$ancFormacion = TRUE){			
		$prp = $this->db->get_where('comte_anuals',$where);
		foreach($prp->result() as $nn=>$pp){
			$prp->row($nn)->foto = base_url('img/comte_anuals/'.$pp->foto);
			$prp->row($nn)->fichero = base_url('files/'.$pp->fichero);
		}
		return $prp;
	}
}